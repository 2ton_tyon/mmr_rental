/**
 * 
 */

$(function(){
	$('#regist').submit(function() {
		var password = $('#password').val();
        var confirmation = $('#confirmPassword').val();
        if (password != confirmation) {
            $('#error').text("パスワードが一致していません。");
            return false;
        } else if (password > 8) {
        	$('#error').text("パスワードが短すぎます。");
            return false;
        } else {
            this.submit();
        }
	});
});