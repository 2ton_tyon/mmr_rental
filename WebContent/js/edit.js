/**
 * 　編集ボタンを押したときにそれぞれの要素をアクティブにする
 * 	また一部の要素をアクティブにし表示する
 */

$(function(){
    $('#cansel').css('display','none');
    $('#regist').css('display','none');
    $('#editPanel').css('display','none');
	
	$("#edit").click(function(){
       $('#regist').css('display','block');
       $('#cansel').css('display','block');
       $('#edit').css('display','none');
       $('#editPanel').css('display','block');
       $('#defaultPanel').css('display','none');
       $('#new_password').css('display', 'block');
       $('#new_confirm_password').css('display', 'block');
    });
	$('#cansel').click(function(){
		$('#regist').css('display','none');
		$('#edit').css('display','block');
		$('#cansel').css('display','none');
		$('#editPanel').css('display','none');
	    $('#defaultPanel').css('display','block');
	    $('#new_password').css('display','none');
		$('#new_confirm_password').css('display','none');
	});
});