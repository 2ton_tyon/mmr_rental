/**
 * カテゴリ追加時、手動でプログラムを追加する必要あり 
 */

$(function(){
	
	$('#category2').css('display', 'none');
	$('#category3').css('display', 'none');
	
	$("#category").change(function() {
	    var extraction_val = $("#category").val();
	    if(extraction_val == "CD") {
	        $('#category1').css('display', 'block');
			$("#category1").removeAttr("disabled");
	        $('#category2').css('display', 'none');
			$("#category2").attr("disabled", "disabled");
			$('#category3').css('display', 'none');
			$("#category3").attr("disabled", "disabled");
	    } else if(extraction_val == "DVD") {
	        $('#category1').css('display', 'none');
			$("#category1").attr("disabled", "disabled");
	        $('#category2').css('display', 'block');
			$("#category2").removeAttr("disabled");
			$('#category3').css('display', 'none');
			$("#category3").attr("disabled", "disabled");
	    } else {
			$('#category1').css('display', 'none');
			$("#category1").attr("disabled", "disabled");
			$('#category2').css('display', 'none');
			$("#category2").attr("disabled", "disabled");
			$('#category3').css('display', 'block');
			$("#category3").removeAttr("disabled");
		}
	})
});