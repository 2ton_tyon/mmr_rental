/**
 * 選択できる日付を表示する
 */
$(function(){
	moment.locale("ja"); //日本語に設定
	var now = moment();
	now.add(1,'d');
	for (var i = 1; i <= 7; i++) {
		var $valDate = now.year() + '-' + now.month() + '-' + now.date();
		var $textDate = now.format("YYYY年 MMM Do dddd");
		now.add(1,'d');
		$("<option>").val($valDate).text($textDate).appendTo('#deliveryDate');
	}
});