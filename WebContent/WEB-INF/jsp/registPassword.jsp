<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="ja">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- jQuery CDN -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<!-- Bootstrap CDN -->
	<link rel="stylesheet"
	    href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet"
	    href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
	<!-- fontawesome CDN -->
	<link rel="stylesheet"
		href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Bootstrap CDN -->
    <script
        src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
        
	<title>MMR ONLINE</title>
</head>
<body>
	<jsp:include page="header.jsp" />
	<div class="row">
		<div class="col-md-offset-3 col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">パスワード再登録</h3>
				</div>
				<div class="panel-body">
					<h3 style="color:red;"><c:out value="${errorMessage}"/></h3>
					<label id="error"></label>
					<form action="reregistPassword?id=<c:out value="${id}" />" method="post">
						<div class="form-group">
							<label><c:out value="新しいパスワード" /></label>
							<input type="password" class="form-control" id="password" name="password" required>
						</div>
						<div class="form-group">
							<label><c:out value="確認用" /></label>
							<input type="password" class="form-control" id="confirmPassword" name="confirmPassword" required>
						</div>
						<button type="submit" class="btn btn-default center-block" id="regist">登録</button>
						<br>
						<button type="button" class="btn btn-default center-block" onclick="location.href='top'">TOPへ</button>
					</form>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="js/check.js"></script>
	<jsp:include page="footer.jsp" />
</body>
</html>