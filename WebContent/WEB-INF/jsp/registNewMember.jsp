<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="ja">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- jQuery CDN -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<!-- Bootstrap CDN -->
	<link rel="stylesheet"
	    href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet"
	    href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
	<!-- fontawesome CDN -->
	<link rel="stylesheet"
		href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Bootstrap CDN -->
    <script
        src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <!--ajaxzip3住所自動入力-->
    <script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>
    
    <style type="text/css">
    <!--
    	.original_box{
    		margin-right: 100px;
    		margin-left: 100px;
    	}
    //-->
    </style>
        
	<title>MMR ONLINE</title>
</head>
<body>
	<jsp:include page="header.jsp" />
	<form action="regist" method="post">
		<div class="panel panel-default original_box">
			<div class="panel-heading">
				<h3 class="panel-title">新規会員登録</h3>
			</div>
			<div class="panel-body">
				<p class="text-center text-danger">
					<c:forEach var="msg" items="${errorMsg}">
						<c:out value="${msg}" />
						<br>
					</c:forEach>
				</p>
				<div class="row">
					<div class="col-md-3">
						<p class="text-right">氏名<font color="red">※</font></p>
					</div>
					<div class="col-md-8">
						<input type="text" name="name" maxlength="40" class="form-control" value="<c:out value="${user.userName}" />" required>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-3">
						<p class="text-right">メールアドレス<font color="red">※</font></p>
					</div>
					<div class="col-md-8">
						<input type="email" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" class="form-control" placeholder="メールアドレスを入力してください" value="<c:out value="${user.mailAddress}" />"  required>
						<br>
						<input type="email" name="confirmEmail" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" class="form-control" placeholder="確認のためもう一度入力してください" value="<c:out value="${user.mailAddress}" />"  required>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-3">
						<p class="text-right">電話番号<font color="red">※</font></p>
					</div>
					<div class="col-md-8">
						<div class="form-group">
							<div class="input-group zip_form">
								<span class="input-group-addon">TEL</span>
								<input type="tel"  pattern="^[0-9]+$" maxlength='11' name="tel" class="form-control" size="11" placeholder="半角(ハイフンなし)で入力してください" value="<c:out value="${user.tel}" />"  required title="電話番号もしくは携帯電話の番号を入力してください">
							</div>
						</div>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-3">
						<p class="text-right">住所<font color="red">※</font></p>
					</div>
					<div class="col-md-8">
						<div>入力後、住所自動表示</div>
							<div class="form-group">
								<div class="input-group zip_form">
									<span class="input-group-addon">〒</span>
									<input type="text" pattern="^[0-9]+$" maxlength='7' name="zip" class="form-control" onKeyUp="AjaxZip3.zip2addr(this,'','address','address');" placeholder="半角(ハイフンなし)で入力してください" value="<c:out value="${user.postalCode}" />"  required>
								</div>
							</div>
							<div class="form-group">
								<div class="input-group zip_form">
									<span class="input-group-addon">住所1</span>
									<input type="text" name="address" size="60" class="form-control" placeholder="市区町村・丁目・番地・号まで入力してください" value="<c:out value="${user.address1}" />"  required>
								</div>
							</div>
							<div class="form-group">
								<div class="input-group zip_form">
									<span class="input-group-addon">住所2</span>
									<input type="text" name="address2" size="60" class="form-control" placeholder="マンション名を入力してください" value="<c:out value="${user.address2}" />"  required>
								</div>
							</div>
						</div>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-3">
						<p class="text-right">生年月日<font color="red">※</font></p>
					</div>
					<div class="col-md-4">
						<input type="date" name="birthday" class="form-control"value="<c:out value="${user.birthdayDate}" />"  required>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-3">
						<p class="text-right">クレジットーカード<font color="red">※</font></p>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<div class="input-group zip_form">
								<span class="input-group-addon">カードNo</span>
								<input type="text" name="cardNo" pattern="^[0-9]+$" maxlength='16' class="form-control" placeholder="半角16桁で入力してください" value="<c:out value="${user.cardNumber}" />"  required>
							</div>
						</div>
						<br>
						<div class="form-group">
							<div class="input-group zip_form">
								<span class="input-group-addon">有効期限</span>
								<input type="text" name="limit" pattern="^[0-9]+$" maxlength='4' class="form-control"  placeholder="(例)0422" value="<c:out value="${user.cardExpirationDate}" />" required>
							</div>
						</div>
						<br>
						<div class="form-group">
							<div class="input-group zip_form">
								<span class="input-group-addon">セキュリティコード</span>
								<input type="text" name="code" pattern="^[0-9]+$" maxlength='4' class="form-control" value="<c:out value="${user.cardSecurityCode}" />"placeholder="半角で入力してください" required>
							</div>
						</div>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-3">
						<p class="text-right">パスワード<font color="red">※</font></p>
					</div>
					<div class="col-md-8">
						<input type="password" name="password" pattern="^[A-Za-z0-9]+$" maxlength='41' class="form-control" placeholder="半角英数字で8文字以上41文字未満で入力してください" required>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-3">
						<p class="text-right">パスワード(確認)<font color="red">※</font></p>
					</div>
					<div class="col-md-8">
						<input type="password" name="confirmPassword" pattern="^[A-Za-z0-9]+$" maxlength='41' class="form-control" placeholder="確認のためもう一度入力してください" required>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-3">
						<p class="text-right">友達招待クーポンID</p>
					</div>
					<div class="col-md-8">
						<input type="text" name="friendCode" class="form-control" value="<c:out value="${user.inviteCodeFriend}" />">
					</div>
				</div>
				<br>
			</div>
		<input type="submit" value="登録" class="btn btn-default center-block">
	</form>
	<br>
	<button type="button" class="btn btn-default center-block" onclick="location.href='top'">TOP</button>
	<jsp:include page="footer.jsp" />
</body>
</html>
