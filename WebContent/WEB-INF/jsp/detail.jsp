<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="ja">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- jQuery CDN -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script> 
	<!-- Bootstrap CDN -->
	<link rel="stylesheet"
	    href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet"
	    href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
	<!-- fontawesome CDN -->
	<link rel="stylesheet"
		href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Bootstrap CDN -->
    <script
        src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <style type="text/css">
    <!--
    	.original_box{
    		margin-right: 100px;
    		margin-left: 100px;
    	}
    //-->
    </style>
	<title>MMR ONLINE</title>
</head>
<body>
	<jsp:include page="header.jsp" />
	<div class="panel panel-default original_box">
		<div class="row">
			<div class="col-md-12">
				<br>
			</div>
			<div class="col-md-3">
				<br>
				<br>
				<a href="#"><img style="width: 200px; height: 200px" src="getImage?id=${ item.item_id }" class= "image-responsive center-block"></a>
			</div>
			<div class="col-md-8">
				<div class="panel panel-default">
					<ul class="list-group">
						<li class="list-group-item"><c:out value="${item.newAndOldName}" /></li>
						<li class="list-group-item"><c:out value="タイトル : ${item.itemName}" /></li>
						<li class="list-group-item"><c:out value="カテゴリ : ${item.categoryName} ジャンル :" />
							<c:forEach var="genre" items="${item.genreName}">
							<c:out value="${genre} " />
							</c:forEach>
						</li>
					  	<li class="list-group-item">
					  		<c:choose>
					  		<c:when test="${item.categoryId == 1}">
					  		<c:out value="アーティスト : ${item.artist}" />
					  		</c:when>
					  		<c:otherwise>
					  		<c:out value="監督 : ${item.artist}" />
					  		</c:otherwise>
					  		</c:choose>
					  	</li>
					  	<li class="list-group-item"><c:out value="料金 : ${item.price}円"/></li>
					  	<li class="list-group-item">
						  	<form action="<c:out value="add?page=${page}&id=${item.item_id}" />" method="post">
		  						<input type="hidden" name="id" value="${item.item_id}">
		  						<button type="submit" class="btn btn-success btn-lg">カートに追加</button>
		  					</form>
					  	</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="panel panel-default original_box">
		<div class="panel-heading">
			<h3 class="panel-title">作品詳細</h3>
		</div>
		<div class="panel-body">
			<c:out value="${item.remarks}" /> 
	  	</div>
	</div>
	<button type="button" class="btn btn-default btn-lg center-block" onclick="location.href='top'">戻る</button>
	<jsp:include page="footer.jsp" />
</body>
</html>