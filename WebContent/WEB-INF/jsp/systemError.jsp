<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="ja">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- jQuery CDN -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<!-- Bootstrap CDN -->
	<link rel="stylesheet"
	    href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet"
	    href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
	<!-- fontawesome CDN -->
	<link rel="stylesheet"
		href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Bootstrap CDN -->
    <script
        src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<title>システムエラー</title>
</head>
<body>
	<jsp:include page="header.jsp" />
	<br>
	<h3 class="text-center">システムエラーが発生しております。<br>お手数ですが、しばらく時間をおいてから再度お試しください。</h3>
	<br>
	<button type="button" class="btn btn-default center-block" onclick="location.href='top'">TOPへ</button>
	<br>
	<c:if test="${!empty sessionScope.userId}">
		<button type="button" class="btn btn-default center-block" onclick="location.href='logout'">ログアウト</button>
	</c:if>
	<jsp:include page="footer.jsp" />
</body>
</html>