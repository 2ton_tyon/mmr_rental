<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- jQuery CDN -->
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<!-- Bootstrap CDN -->
<link rel="stylesheet"
	href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet"
	href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
<!-- fontawesome CDN -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap CDN -->
<script
	src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.0/moment-with-locales.js"></script>

<title>MMR ONLINE</title>
</head>
<body>
	<jsp:include page="header.jsp" />
	<form action="rental" method="post">
		<div class="row">
			<div class="col-md-offset-3 col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">レンタル詳細</h3>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<label>お届け希望日時<font color="red">※</font></label>
							<select id="deliveryDate" name="deliveryDate" class="form-control"required>
							</select>
						</div>
						<br>
						<div class="form-group">
							<label>受け取り希望時間<font color="red">※</font></label>
							<select name="time" class="form-control" required>
								<option value="1">午前中</option>
								<option value="2">12:00 - 15:00</option>
								<option value="3">15:00 - 18:00</option>
								<option value="4">18:00 - 21:00</option>
							</select>
						</div>
						<br>
						<div class="form-group">
							<label>クーポンの利用</label>※クーポンは会計時に反映されます。
							<select name="coupon" size="1" class="form-control" id="coupon">
								<option value="0" selected>---</option>
								<c:forEach var="c" items="${coupon}">
									<option value="${c.couponId}"><c:out
											value="${c.couponContent}" /></option>
								</c:forEach>
							</select>
						</div>
						<br> <input type="hidden" id="fee" value="<c:out value="${accounting}"/>">
						<h3>
							<label id=accounting><c:out value="お会計金額 : ${accounting}円" /></label>
						</h3>
						<br>
						<button type="submit" class="btn btn-default center-block">確定</button>
						<br>
						<button type="button" class="btn btn-default center-block" onclick="location.href='cart'">戻る</button>
					</div>
				</div>
			</div>
		</div>
	</form>
	<script>
	<!--
	$("#coupon").change(function() {
	    var coupon_id = $("#coupon").val();
		var _default = parseInt($("#fee").val());
		var discount_rate;
		if (coupon_id == 0) {
			$('#accounting').text("お会計金額 : " + _default + "円");
		}
		<c:forEach var="c" items="${coupon}">
			if (${c.couponId} == coupon_id) {
				$('#accounting').text("お会計金額 : " + ((100 - parseInt(${coupon.get(i).discountRate}) ) / 100 ) * _default + "円");
			}
		</c:forEach>
	});
	-->
	</script>
	<script type="text/javascript" src="js/deliveryDate.js"></script>
	<jsp:include page="footer.jsp" />
</body>
</html>