<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="ja">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- jQuery CDN -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<!-- Bootstrap CDN -->
	<link rel="stylesheet"
	    href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet"
	    href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
	<!-- fontawesome CDN -->
	<link rel="stylesheet"
		href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Bootstrap CDN -->
    <script
        src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <!--ajaxzip3住所自動入力-->
    <script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>
    
    <style type="text/css">
    <!--
    	.original_box{
    		margin-right: 100px;
    		margin-left: 100px;
    	}
    //-->
    </style>
        
	<title>MMR ONLINE</title>
</head>
<body>
	<jsp:include page="header.jsp" />
	<div class="original_box">
		<h2 class="text-left"><c:out value="${user.userName}さんのマイページ" /></h2>
		<br>
		<p class="text-left">招待コード:<c:out value="${user.inviteCode}" /></p>
	</div>
	<hr>
	<form action="updateUser" method="post">
		<div class="panel panel-info original_box">
			<div class="panel-heading">
				<div class="panel-title" style="color:black;">
					ユーザ情報&nbsp;&nbsp;&nbsp;
					<button type="button" class="right-block btn btn-default" value="編集" id="edit">編集</button>
				</div>
			</div>
			<div class="panel-body">
				<div class="text-center">
					<c:out value="${sessionScope.msg}"/>
					<c:remove var="msg" scope="session"/>
				</div>
				<c:if test="${!empty sessionScope.errorMsg}">
					<center>
						<font color="red">
							<c:forEach var="eMsg" items="${sessionScope.errorMsg}">
								<c:out value="${eMsg}" />
								<br>
							</c:forEach>
						</font>
					</center>
					<c:remove var="errorMsg" scope="session"/>
				</c:if>
				<div id="editPanel" style="display:none;">
					<div class="row">
						<div class="col-md-3">
							<p class="text-right">氏名<font color="red">※</font></p>
						</div>
						<div class="col-md-9">
							<input type="text" name="name" class="form-control" value="<c:out value="${user.userName}" />" required>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-3">
							<p class="text-right">メールアドレス<font color="red">※</font></p>
						</div>
						<div class="col-md-9">
							<input type="email" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" class="form-control" placeholder="メールアドレスを入力してください" value="<c:out value="${user.mailAddress}" />"  required>
							<br>
							<input type="email" name="confirmEmail" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" class="form-control" placeholder="確認のためもう一度入力してください" value="<c:out value="${user.mailAddress}" />"  required>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-3">
							<p class="text-right">電話番号<font color="red">※</font></p>
						</div>
						<div class="col-md-9">
							<div class="input-group zip_form">
								<span class="input-group-addon">TEL</span>
								<input type="tel"  pattern="^[0-9]+$" maxlength='11' name="tel" class="form-control" size="11" placeholder="半角(ハイフンなし)で入力してください" value="<c:out value="${user.tel}" />"  required title="電話番号もしくは携帯電話の番号を入力してください">
							</div>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-3">
							<p class="text-right">住所<font color="red">※</font></p>
						</div>
						<div class="col-md-9">
							<div class="input-group zip_form">
								<span class="input-group-addon">〒</span>
								<input type="text" pattern="^[0-9]+$" maxlength='7' name="zip" class="form-control" onKeyUp="AjaxZip3.zip2addr(this,'','address','address');" placeholder="半角(ハイフンなし)で入力してください" value="<c:out value="${user.postalCode}" />"  required>
							</div>
							<br>
							<div class="input-group zip_form">
								<span class="input-group-addon">住所1</span>
								<input type="text" name="address" size="60" class="form-control" placeholder="市区町村・丁目・番地・号まで入力してください" value="<c:out value="${user.address1}" />"  required>
							</div>
							<br>
							<div class="input-group zip_form">
								<span class="input-group-addon">住所2</span>
								<input type="text" name="address2" size="60" class="form-control" placeholder="マンション名を入力してください" value="<c:out value="${user.address2}" />"  required>
							</div>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-3">
							<p class="text-right">クレジットーカード<font color="red">※</font></p>
						</div>
						<div class="col-md-9">
							<div class="input-group zip_form">
								<span class="input-group-addon">カードNo</span>
								<input type="text" name="cardNo" pattern="^[0-9]+$" maxlength='16' class="form-control" placeholder="半角16桁で入力してください" value="<c:out value="${user.cardNumber}" />"  required>
							</div>
							<br>
							<div class="input-group zip_form">
								<span class="input-group-addon">有効期限</span>
								<input type="text" name="limit" pattern="^[0-9]+$" maxlength='4' class="form-control"  placeholder="(例)0422" value="<c:out value="${user.cardExpirationDate}" />" required>
							</div>
							<br>
							<div class="input-group zip_form">
								<span class="input-group-addon">セキュリティコード</span>
								<input type="text" name="code" pattern="^[0-9]+$" maxlength='4' class="form-control" value="<c:out value="${user.cardSecurityCode}" />"placeholder="半角で入力してください" required>
							</div>
						</div>
					</div>
					<br>
					<div class="row" id="new_password" style="display:none">
						<div class="col-md-3">
							<p class="text-right">新しいパスワード</p>
						</div>
						<div class="col-md-9">
							<input type="password" name="password" class="form-control" pattern="^[A-Za-z0-9]+$" maxlength='41' placeholder="※パスワードは変更する場合のみ入力してください">
						</div>
					</div>
					<br>
					<div class="row" id="new_confirm_password" style="display:none">
						<div class="col-md-3">
							<p class="text-right">新しいパスワード(確認)</p>
						</div>
						<div class="col-md-9">
							<input type="password" name="confirmPassword" pattern="^[A-Za-z0-9]+$" maxlength='41' class="form-control">
						</div>
					</div>
					<br>
				</div>
				<div id="defaultPanel">
					<div class="row">
						<div class="col-md-3">
							<p class="text-right">氏名 :</p>
						</div>
						<div class="col-md-9">
							<label><c:out value="${user.userName}"/></label>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-3">
							<p class="text-right">メールアドレス :</p>
						</div>
						<div class="col-md-9">
							<label><c:out value="${user.mailAddress}"/></label>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-3">
							<p class="text-right">電話番号 :</p>
						</div>
						<div class="col-md-9">
							<label><c:out value="${user.tel}"/></label>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-3">
							<p class="text-right">住所 :</p>
						</div>
						<div class="col-md-9">
							<label>〒 : <c:out value="${user.postalCode}"/></label>
							<hr>
							<label>住所1 :<c:out value="${user.address1}"/></label>
							<hr>
							<label>住所2 :<c:out value="${user.address2}"/></label>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-3">
							<p class="text-right">生年月日 :</p>
						</div>
						<div class="col-md-9">
							<label><c:out value="${user.birthdayDate}"/></label>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-3">
							<p class="text-right">クレジットーカード :</p>
						</div>
						<div class="col-md-9">
							<label>
								カートNo
								<br>
								<c:out value="${user.cardNumber}"/>
							</label>
							<br>
							<hr>
							<label>
								有効期限
								<br>
								<c:out value="${user.cardExpirationDate}"/>
							</label>
							<br>
							<hr>
							<label>
								セキュリティコード
								<br>
								<c:out value="${user.cardSecurityCode}"/>
							</label>
						</div>
					</div>
				</div>
			</div>
		</div>
		<button type="button" class="btn btn-default center-block" id="cansel">キャンセル</button>
		<br>
		<button type="submit" value="登録" class="btn btn-default center-block" id="regist">登録</button>
	</form>
	<br>
	<c:choose>
		<c:when test="${empty user.coupons}">
		<center><label>現在、所持クーポンはありません</label></center>
		</c:when>
		<c:otherwise>
			<div class="panel panel-info original_box">
				<div class="panel-heading">
					<div class="panel-title" style="color:black;">
						クーポン一覧
					</div>
				</div>
				<div class="panel-body">
					<div class="table-responsiv original_box">
						<table class="table table-bordered table-hover">
							<tr>
								<th>クーポン名</th>
								<th>内容</th>
								<th>有効期限</th>
							</tr>
							<c:forEach var="coupon" items="${user.coupons}">
							<tr>
								<td>友達招待クーポン</td>
								<td><c:out value="${coupon.couponContent}" /></td>
								<td><c:out value="${coupon.activeFrom} - ${coupon.activeUntil}" /></td>
							</tr>
							</c:forEach>
						</table>
					</div>
				</div>
			</div>
		</c:otherwise>
	</c:choose>
	<br>
	<h2 class="original_box text-center"><a href="orderHistory" class="">注文履歴</a></h2>
	<br>
	<button type="button" class="btn btn-default center-block" onclick="location.href='top'">TOPへ</button>
	<script type="text/javascript" src="js/edit.js"></script>
	<jsp:include page="footer.jsp" />
</body>
</html>