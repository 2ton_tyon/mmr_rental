<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="ja">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- jQuery CDN -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<!-- Bootstrap CDN -->
	<link rel="stylesheet"
	    href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet"
	    href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
	<!-- fontawesome CDN -->
	<link rel="stylesheet"
		href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Bootstrap CDN -->
    <script
        src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
        
	<title>MMR ONLINE</title>
</head>
<body>
	<jsp:include page="header.jsp" />
	<h2><font color="#3B170B"><c:out value="★おススメ" /></font></h2>
	<div class="row">
		<c:forEach items="${ requestScope.itemList }" var="item" varStatus="status">
		<c:choose>
			<c:when test="${status.index %5 == 0}">
				<div class="col-md-12"><br><br><br></div>
				<div class="col-md-offset-1 col-md-2">
					<a href="detail?id=${item.item_id }"><img style="width: 200px; height: 200px" src="getImage?id=${ item.item_id }" class= "image-responsive center-block"></a>
					<br>
					<form action="<c:out value="add?page=top&id=${item.item_id}" />" method="post">
  						<button type="submit" class="btn btn-success btn-lg center-block">カートに追加</button>
	  				</form>
				</div>
			</c:when>
			<c:otherwise>
				<div class="col-md-2">
					<a href="detail?id=${item.item_id }"><img style="width: 200px; height: 200px" src="getImage?id=${ item.item_id }" class= "image-responsive center-block"></a>
					<br>
					<form action="<c:out value="add?page=top&id=${item.item_id}" />" method="post">
  						<button type="submit" class="btn btn-success btn-lg center-block">カートに追加</button>
	  				</form>
				</div>
			</c:otherwise>
		</c:choose>
		</c:forEach>
	</div>
	<c:if test="${!empty sessionScope.userId}">
		<hr style="border-width: 8px 0 0 0;border-color:#3B170B;">
		<h2><a href="orderHistory" class=""><font color="#3B170B"><c:out value="★注文履歴" /></font></a></h2>
	</c:if>
	<div class="row">
		<c:forEach items="${ requestScope.orderList }" var="orderItem" varStatus="status">
		<c:choose>
			<c:when test="${status.index %5 == 0}">
				<div class="col-md-offset-1 col-md-2">
					<a href="detail?id=${orderItem.item_id}"><img style="width: 200px; height: 200px" src="getImage?id=${ orderItem.item_id }" class= "image-responsive center-block"></a>
					<br>
					<form action="<c:out value="add?page=top&id=${orderItem.item_id}" />" method="post">
  						<button type="submit" class="btn btn-success btn-lg center-block">カートに追加</button>
	  				</form>
				</div>
			</c:when>
			<c:otherwise>
				<div class="col-md-2">
					<a href="detail?id=${orderItem.item_id }"><img style="width: 200px; height: 200px" src="getImage?id=${ orderItem.item_id }" class= "image-responsive center-block"></a>
					<br>
					<form action="<c:out value="add?page=top&id=${orderItem.item_id}" />" method="post">
  						<button type="submit" class="btn btn-success btn-lg center-block">カートに追加</button>
	  				</form>
				</div>
			</c:otherwise>
		</c:choose>
		</c:forEach>
	</div>
	<jsp:include page="footer.jsp" />
</body>
</html>