<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="ja">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- jQuery CDN -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<!-- Bootstrap CDN -->
	<link rel="stylesheet"
	    href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet"
	    href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
	<!-- fontawesome CDN -->
	<link rel="stylesheet"
		href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Bootstrap CDN -->
    <script
        src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    
    <style type="text/css">
    <!--
    	.original_box{
    		margin-right: 100px;
    		margin-left: 100px;
    	}
    //-->
    </style>
        
	<title>MMR ONLINE</title>
</head>
<body>
	<jsp:include page="header.jsp" />
	<h2><c:out value="検索結果 ${category}<${genre} : ${title}  検索商品件数  : ${cntItem}件" /></h2>
	<div class="text-center">
		<ul class="pagination justify-content-center pagination-lg">
			<c:choose>
			<c:when test="${page == 1}">
			<li class="page-item disabled">
				<a class="page-link">&laquo;</a>
			</li>
			</c:when>
			<c:otherwise>
			<li class="page-item">
				<a class="page-link" href="<c:out value="Search?page=${page-1}&category=${category}&genre=${genre}&search=${title}" />">&laquo;</a>
			</li>
			</c:otherwise>
			</c:choose>
			<li class="page-item">
				<a class="page-link" href="<c:out value="Search?page=${page}&category=${category}&genre=${genre}&search=${title}" />"><c:out value="${page}"/></a>
			</li>
			<c:choose>
			<c:when test="${(cntItem - (page*10)) > 0}">
			<li class="page-item">
				<a class="page-link" href="<c:out value="Search?page=${page+1}&category=${category}&genre=${genre}&search=${title}" />">&raquo;</a>
			</li>
			</c:when>
			<c:otherwise>
			<li class="page-item disabled">
				<a class="page-link">&raquo;</a>
			</li>
			</c:otherwise>
			</c:choose>
	  	</ul>
	</div>
	<c:forEach var="item" items="${itemList}">
	<div class="panel panel-default original_box">
	  <div class="panel-body">
	  	<div class="row">
	  		<div class="col-md-3">
	  			<a href="detail?id=${item.item_id}"><img style="width: 200px; height: 200px" src="getImage?id=${ item.item_id}" class= "image-responsive"></a>
	  		</div>
	  		<div class="col-md-9">
	  			<div class="row">
	  				<div class="col-md-1">
	  					<c:choose>
	  					<c:when test="${item.newAndOldId == 1}">
	  					<p class="label label-danger"><c:out value="${item.newAndOldName}" /></p>
	  					</c:when>
	  					<c:when test="${item.newAndOldId == 2}">
	  					<p class="label label-primary"><c:out value="${item.newAndOldName}" /></p>
	  					</c:when>
	  					<c:otherwise>
	  					<p class="label label-default"><c:out value="${item.newAndOldName}" /></p>
	  					</c:otherwise>
	  					</c:choose>
	  				</div>
	  				<div class="col-md-2">
	  					<c:out value="${item.price}円" />
	  				</div>
	  			</div>
	  			<h3><a href="detail?id=${item.item_id}"><font color="#000000"><c:out value="${item.itemName}" /></font></a></h3>
	  			<hr>
	  			<div class="row">
	  				<div class="col-md-5">
	  					<c:choose>
	  					<c:when test="${item.categoryId==1}">
	  					<c:out value="アーティスト:${item.artist}" />
	  					</c:when>
	  					<c:otherwise>
	  					<c:out value="監督:${item.artist}" />
	  					</c:otherwise>
	  					</c:choose>
	  					<br>
	  					<c:out value="カテゴリ:${item.categoryName}" />
	  					<br>
	  					ジャンル:
	  					<c:forEach var="genre" items="${item.genreName}" varStatus="status">
	  					<c:choose>
	  					<c:when test="${status.last}">
	  					<c:out value="${genre}" />
	  					</c:when>
	  					<c:otherwise>
	  					<c:out value="${genre} , " />
	  					</c:otherwise>
	  					</c:choose>
	  					</c:forEach>
	  				</div>
	  				<div class="col-md-4">
	  					<form action="<c:out value="add?page=${page}&category=${category}&genre=${genre}&search=${title}" />" method="post">
	  						<input type="hidden" name="id" value="${item.item_id}">
	  						<button type="submit" class="btn btn-success btn-lg">カートに追加</button>
	  					</form>
	  				</div>
	  			</div>
	  		</div>
	  	</div>
	  </div>
	</div>
	</c:forEach>
	<c:choose>
	<c:when test="${cntItem == 0}">
	<h3>検索条件に合う商品はありません。</h3>
	</c:when>
	<c:otherwise>
	<div class="text-center">
		<ul class="pagination justify-content-center pagination-lg">
			<c:choose>
			<c:when test="${page == 1}">
			<li class="page-item disabled">
				<a class="page-link">&laquo;</a>
			</li>
			</c:when>
			<c:otherwise>
			<li class="page-item">
				<a class="page-link" href="<c:out value="Search?page=${page-1}&category=${category}&genre=${genre}&search=${title}" />">&laquo;</a>
			</li>
			</c:otherwise>
			</c:choose>
			<li class="page-item">
				<a class="page-link" href="<c:out value="Search?page=${page}&category=${category}&genre=${genre}&search=${title}" />"><c:out value="${page}"/></a>
			</li>
			<c:choose>
			<c:when test="${(cntItem - (page*10)) > 0}">
			<li class="page-item">
				<a class="page-link" href="<c:out value="Search?page=${page+1}&category=${category}&genre=${genre}&search=${title}" />">&raquo;</a>
			</li>
			</c:when>
			<c:otherwise>
			<li class="page-item disabled">
				<a class="page-link">&raquo;</a>
			</li>
			</c:otherwise>
			</c:choose>
	  	</ul>
	</div>
	</c:otherwise>
	</c:choose>
	<jsp:include page="footer.jsp" />
</body>
</html>