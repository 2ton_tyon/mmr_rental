<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page pageEncoding="UTF-8" %>
<br>
<div class="row">
	<div class="col-md-3">
		<a href="top"><img src="img/MMR-logo-2.png" class="image-responsive center-block"></a>
	</div>
	<div class="col-md-9">
		<div class="row">
			<form action="login" method="post">
				<div class="col-md-offset-2 col-md-3">
					<c:if test="${empty sessionScope.userId}">
					<div class="form-group">
						<label><c:out value="メールアドレス" /></label>
						<input type="text" name="userId" class="form-control">
					</div>
					</c:if>
				</div>
				<div class="col-md-3">
					<c:if test="${empty sessionScope.userId}">
						<div class="form-group">
						<label><c:out value="パスワード" /></label>
						<input type="password" name="password" class="form-control">
					</div>
					</c:if>
				</div>
				<div class="col-md-3">
					<div class="row">
						<div class="col-md-12">
							<br>
						</div>
						<c:choose>
						<c:when test="${empty sessionScope.userId}">
						<div class="col-md-3">
							<button type="submit" class="btn btn-primary center-block">ログイン</button>
						</div>
						<div class="col-md-offset-4 col-md-3">
							<button type="button" class="btn btn-primary center-block" onclick="location.href='regist'">新規会員登録</button>
						</div>
						</c:when>
						<c:otherwise>
						<div class="col-md-3">
							<button type="button" class="btn btn-primary center-block" onclick="location.href='logout'">ログアウト</button>
							<br>
							<br>
						</div>
						<div class="col-md-offset-4 col-md-3">
							<button type="button" class="btn btn-primary center-block" onclick="location.href='user_page'">マイページ</button>
							<br>
							<br>
						</div>
						</c:otherwise>
						</c:choose>
					</div>
				</div>
			</form>
			<div class="col-md-12">
			</div>
			<form action="Search" method="get">
				<div class="col-md-offset-2 col-md-2">
					<select name="category" size="1" class="form-control" id="category">
					<c:forEach var="category" items="${sessionScope.category}">
						<option value="${category}"><c:out value="${category}" /></option>
					</c:forEach>
					</select>
				</div>
				<div class="col-md-2">
				<c:forEach var="category" items="${sessionScope.category}" varStatus="status">
				<c:choose>
				<c:when test="${status.index==0}">
					<select name="genre" size="1" class="form-control" id="<c:out value="category${status.index+1}" />">
					<c:forEach var="genre" items="${sessionScope.allGenreList[category]}">
						<option value="${genre}"><c:out value="${genre}" /></option>
					</c:forEach>
					</select>
				</c:when>
				<c:otherwise>
					<select name="genre" size="1" class="form-control" id="<c:out value="category${status.index+1}" />" disabled>
					<c:forEach var="genre" items="${sessionScope.allGenreList[category]}">
						<option value="${genre}"><c:out value="${genre}" /></option>
					</c:forEach>
					</select>
				</c:otherwise>
				</c:choose>
				</c:forEach>
				</div>
				<div class="col-md-3">
					<input name="search" type="text" class="form-control" placeholder="商品名を入力してください">
				</div>
				<div class="col-md-1">
					<button type="submit" class="btn btn-success center-block">検索</button>
				</div>
			</form>
			<c:if test="${!empty sessionScope.cart}">
				<div class="col-md-1">
					<a href="cart" class="btn btn-primary">
					  <i class="fa fa-shopping-cart fa-2x" aria-hidden="true"></i>
					</a>
				</div>
			</c:if>
		</div>
	</div>
</div>
<script type="text/javascript" src="js/select.js"></script>
<hr style="border-width: 8px 0 0 0;border-color:#3B170B;">