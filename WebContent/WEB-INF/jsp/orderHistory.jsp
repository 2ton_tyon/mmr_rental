<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="ja">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- jQuery CDN -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<!-- Bootstrap CDN -->
	<link rel="stylesheet"
	    href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet"
	    href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
	<!-- fontawesome CDN -->
	<link rel="stylesheet"
		href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Bootstrap CDN -->
    <script
        src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    
    <style type="text/css">
    <!--
    	.original_box{
    		margin-right: 100px;
    		margin-left: 100px;
    	}
    //-->
    </style>
        
	<title>MMR ONLINE</title>
</head>
<body>
	<jsp:include page="header.jsp" />
	<h2><c:out value="★注文履歴" /></h2>
	
	<c:forEach items="${requestScope.oldorderstatus}" var="oldorder" varStatus="status">	
		<div class="panel panel-default original_box">
			<div class="panel-body">
				<div class="panel panel-default">
					<div class="row">
						<div class="col-md-2">
							<label>レンタル日時</label>
							<br>
							<c:out value="${oldorder.orderDate}"/>
						</div>
						<div class="col-md-2">
							<label>返却日</label>
							<br>
							<c:out value="${oldorder.returnDate}"/>
						</div>
						<div class="col-md-1">
							<label>数量</label>
							<br>
							<c:out value="${oldorder.itemSum}"/>
						</div>
						<div class="col-md-2">
							<label>お会計金額</label>
							<br>
							<c:out value="${oldorder.fee}円"/>
						</div>
						<div class="col-md-2">
							<br>
							<c:out value="${oldorder.status}"/>
						</div>
					</div>
				</div>
				<c:forEach items="${oldorder.item}" var="oldorderdetail" varStatus="status">
				<div class="panel panel-default">
					<div class="panel-body">
					  	<div class="row">
					  		<div class="col-md-3">
					  			<a href="detail?id=${oldorderdetail.item_id}"><img style="width: 200px; height: 200px" src="getImage?id=${ oldorderdetail.item_id }" class= "image-responsive"></a>
					  		</div>
					  		<div class="col-md-9">
					  			<div class="row">
					  				<div class="col-md-1">
					  					<p class="label label-danger"><c:out value="${oldorderdetail.newAndOldName}" /></p>
					  				</div>
					  				<div class="col-md-2">
					  					<c:out value="${oldorderdetail.price}円" />
					  				</div>
					  			</div>
					  			<h3><a href="#"><font color="#000000"><c:out value="${oldorderdetail.itemName}" /></font></a></h3>
					  			<hr>
					  			<div class="row">
					  				<div class="col-md-6">
					  					アーティスト:<c:out value="${oldorderdetail.artist}"/>
					  					<br>
					  					カテゴリ:<c:out value="${oldorderdetail.categoryName} "/>
					  					<br>
					  					ジャンル：
					  					<c:forEach var="genre" items="${oldorderdetail.genreName}" varStatus="status">
						  					<c:choose>
							  					<c:when test="${status.last}">
							  						<c:out value="${genre}" />
							  					</c:when>
							  					<c:otherwise>
							  						<c:out value="${genre} , " />
							  					</c:otherwise>
						  					</c:choose>
					  					</c:forEach>
					  				</div>
					  			</div>
					  		</div>
					  	</div>
					</div>
				</div>
			</c:forEach>
			</div>
		</div>
		<hr class="original_box" style="border-width: 4px 0 0 0;border-color:green;">
	</c:forEach>
	<c:if test="${empty oldorderstatus}">
	<h3>注文履歴はありません。</h3>
	</c:if>
	<jsp:include page="footer.jsp" />
</body>
</html>