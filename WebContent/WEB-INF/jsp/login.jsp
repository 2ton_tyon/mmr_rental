<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ja">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- jQuery CDN -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<!-- Bootstrap CDN -->
	<link rel="stylesheet"
	    href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet"
	    href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
	<!-- fontawesome CDN -->
	<link rel="stylesheet"
		href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Bootstrap CDN -->
    <script
        src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<style type="text/css">
	<!--
		.pos{
			position: relative;
			left: 520px;
		}
	//-->
	</style>
	<title>MMR ONLINE</title>
</head>
<body>
	<jsp:include page="header.jsp" />
	<br>
	<br>
	<br>
	<center><img src="img/MMR-logo-2.png" class="img-responsive"></center>
	<br>
	<c:if test="${!empty sessionScope.msg }">
	<center><c:out value="${sessionScope.msg}" /></center>
	<c:remove var="msg" scope="session"/>
	</c:if>
	<center><font color="red"><c:out value="${errorMessage}" /></font></center>
	<form action="login" method="post">
	<div class="col-md-offset-3 col-md-6">
				<div class="form-group">
					<label><c:out value="ID" /></label>
					<input type="text" class="form-control" name="userId">
				</div>
			</div>
		
	<div class="row">
		<div class="col-md-offset-3 col-md-6">
				<div class="form-group">
					<label><c:out value="パスワード" /></label>
					<input type="password" maxlength="41" class="form-control" name="password">
				</div>
			</div>
	</div>
	<div class="row">
		<div class="col-md-offset-3 col-md-6">
				<div class="form-group pull-right">
					<label><a href="reregistPassword">パスワードを忘れた場合はこちら</a></label>
				</div>
			</div>
	</div>
	<center><button type="submit" name="login" class="btn btn-primary">ログイン</button></center>
	</form>
	<br>
	<br>
	<br>
	<center>
	<form action="regist" method="get">
		<button type="submit" class="btn btn-primary">会員登録をお済みでない方はこちら</button>
	</form>
	</center>
	<br>
	<br>
	<br>
	<br>
	<br>
	<jsp:include page="footer.jsp" />
	</body>
</html>