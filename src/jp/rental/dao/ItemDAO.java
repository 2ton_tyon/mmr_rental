package jp.rental.dao;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import jp.rental.SQLRuntimeException;
import jp.rental.dto.ItemDTO;

public class ItemDAO {
	
	private Connection connection;
	
	/**
	 * コネクションをセットする
	 * @param connection
	 */
	public ItemDAO(Connection connection) {
		this.connection = connection;
	}
	
	/**
	 * 引数で指定された数に応じたおススメ商品をランダムに抽出して取得する
	 * @param getNum　商品数
	 * @return 商品一覧
	 * @throws SQLRuntimeException　実行時例外
	 */
	public ArrayList<ItemDTO> selectRecommendItem(int getNum) throws SQLRuntimeException {
		
		// SQL文作成
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        ITEM_ID");
		sb.append("    FROM");
		sb.append("        ITEM");
		sb.append("    WHERE");
		sb.append("        RECOMMENDED_FLG = 1");
		sb.append("    ORDER BY");
		sb.append("        RAND() LIMIT 0");
		sb.append("        ,?");
		
		// リスト作成
		ArrayList<ItemDTO> list = new ArrayList<>();
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setInt(1, getNum);
			ResultSet rs = ps.executeQuery();
			
			// SQLの結果を取得し、リストに詰める
			while (rs.next()) {
				ItemDTO imageData = new ItemDTO();
				
				imageData.setItem_id(rs.getInt("item_id"));
				list.add(imageData);
			}
			return list;
			
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} 
	}
	
	/**
	 * 商品IDに紐づいた商品画像を取得する
	 * @param id 商品ID
	 * @return　商品イメージ
	 * @throws SQLRuntimeException　実行時例外
	 */
	public BufferedImage selectImageById(int id) throws SQLRuntimeException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append("   SELECT");
		sb.append("          IMAGE");
		sb.append("     FROM ITEM");
		sb.append("    WHERE ITEM_ID = ?");
		
		try {
			// SQL文を実行する
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
		
			BufferedInputStream bis = null;
			// 結果セットから画像データを取得し、返却する
			if (rs.next()) {
				InputStream is = rs.getBinaryStream("image");
				bis = new BufferedInputStream(is);
			}
			return ImageIO.read(bis);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} catch (IOException e) {
			throw new SQLRuntimeException(e);
		}
	}
	
	/**
	 * 引数に該当する商品の一覧を取得する
	 * @param page ページ数
	 * @param category　カテゴリー名
	 * @param genre　ジャンル名
	 * @param title　商品名
	 * @return 商品一覧
	 * @throws SQLRuntimeException 実行時例外
	 */
	public ArrayList<ItemDTO> selectSearchItem(int page,String category, String genre, String title) throws SQLRuntimeException{
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        ITEM_ID");
		sb.append("        ,ITEM_NAME");
		sb.append("        ,ITEM.CATEGORY_ID");
		sb.append("        ,CATEGORY.CATEGORY_NAME");
		sb.append("        ,ITEM.NEW_AND_OLD_ID");
		sb.append("        ,NEW_AND_OLD.NEW_AND_OLD_NAME");
		sb.append("        ,ARTIST");
		sb.append("        ,PRICE");
		sb.append("    FROM");
		sb.append("        ITEM");
		sb.append("            LEFT JOIN CATEGORY");
		sb.append("                ON ITEM.CATEGORY_ID = CATEGORY.CATEGORY_ID");
		sb.append("            LEFT JOIN NEW_AND_OLD");
		sb.append("                ON ITEM.NEW_AND_OLD_ID = NEW_AND_OLD.NEW_AND_OLD_ID");
		sb.append("    WHERE");
		sb.append("        ITEM_ID IN (");
		sb.append("            SELECT");
		sb.append("                    DISTINCT ITEM.ITEM_ID");
		sb.append("                FROM");
		sb.append("                    ITEM_GENRE");
		sb.append("                        LEFT JOIN ITEM");
		sb.append("                            ON ITEM_GENRE.ITEM_ID = ITEM.ITEM_ID");
		sb.append("                        LEFT JOIN CATEGORY");
		sb.append("                            ON ITEM.CATEGORY_ID = CATEGORY.CATEGORY_ID");
		sb.append("                        LEFT JOIN GENRE");
		sb.append("                            ON GENRE.GENRE_ID = ITEM_GENRE.GENRE_ID");
		sb.append("                WHERE");
		sb.append("                    ITEM.ITEM_NAME LIKE ?");
		sb.append("                    AND CATEGORY.CATEGORY_NAME = ?");
		sb.append("                    AND GENRE.GENRE_NAME = ?");
		sb.append("        )");
		sb.append("    ORDER BY");
		sb.append("        ITEM_ID DESC LIMIT ?");
		sb.append("        ,10");
	
		StringBuffer sb2 = new StringBuffer();
		sb2.append("SELECT");
		sb2.append("        GENRE.GENRE_NAME");
		sb2.append("    FROM");
		sb2.append("        ITEM_GENRE");
		sb2.append("            LEFT JOIN GENRE");
		sb2.append("                ON GENRE.GENRE_ID = ITEM_GENRE.GENRE_ID");
		sb2.append("    WHERE");
		sb2.append("        ITEM_ID = ?");
		
		ArrayList<ItemDTO> itemList = new ArrayList<>();
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setString(1,  String.format("%1$s%2$s%1$s", "%", title));
			ps.setString(2, category);
			ps.setString(3, genre);
			ps.setInt(4, (page-1)*10);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				ItemDTO item = new ItemDTO();
				item.setItem_id(rs.getInt("ITEM_ID"));
				item.setItemName(rs.getString("ITEM_NAME"));
				item.setCategoryId(rs.getInt("ITEM.CATEGORY_ID"));
				item.setCategoryName(rs.getString("CATEGORY.CATEGORY_NAME"));
				item.setNewAndOldId(rs.getInt("ITEM.NEW_AND_OLD_ID"));
				item.setNewAndOldName(rs.getString("NEW_AND_OLD.NEW_AND_OLD_NAME"));
				item.setArtist(rs.getString("ARTIST"));
				item.setPrice(rs.getInt("PRICE"));
				
				PreparedStatement ps2 = connection.prepareStatement(sb2.toString());
				ps2.setInt(1, item.getItem_id());
				ResultSet rs2 = ps2.executeQuery();

				ArrayList<String> genreList = new ArrayList<>();
				while (rs2.next()) {
					genreList.add(rs2.getString("GENRE.GENRE_NAME"));
				}
				item.setGenreName(genreList);
				itemList.add(item);
			}
			return itemList;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}
	
	/**
	 * 商品IDに紐づいた商品の詳細を取得する
	 * @param id 商品ID
	 * @return　商品詳細
	 * @throws SQLRuntimeException 実行時例外
	 */
	public ItemDTO selectItemDetail(int id) throws SQLRuntimeException{
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        ITEM_ID");
		sb.append("        ,ITEM_NAME");
		sb.append("        ,ITEM.CATEGORY_ID");
		sb.append("        ,CATEGORY.CATEGORY_NAME");
		sb.append("        ,ITEM.NEW_AND_OLD_ID");
		sb.append("        ,NEW_AND_OLD.NEW_AND_OLD_NAME");
		sb.append("        ,ARTIST");
		sb.append("        ,PRICE");
		sb.append("        ,REMARKS");
		sb.append("    FROM");
		sb.append("        ITEM");
		sb.append("            LEFT JOIN CATEGORY");
		sb.append("                ON CATEGORY.CATEGORY_ID = ITEM.CATEGORY_ID");
		sb.append("            LEFT JOIN new_and_old");
		sb.append("                ON NEW_AND_OLD.NEW_AND_OLD_ID = ITEM.NEW_AND_OLD_ID");
		sb.append("    WHERE");
		sb.append("        ITEM_ID = ?");
		
		StringBuffer sb2 = new StringBuffer();
		sb2.append("   SELECT");
		sb2.append("          GENRE.GENRE_NAME");
		sb2.append("    FROM ITEM_GENRE");
		sb2.append("    LEFT JOIN GENRE");
		sb2.append("    ON GENRE.GENRE_ID = ITEM_GENRE.GENRE_ID");
		sb2.append("    WHERE ITEM_ID = ?");
		
		ItemDTO item = new ItemDTO();
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			
			if (rs.next()) {
				item.setItem_id(rs.getInt("ITEM_ID"));
				item.setItemName(rs.getString("ITEM_NAME"));
				item.setCategoryId(rs.getInt("ITEM.CATEGORY_ID"));
				item.setCategoryName(rs.getString("CATEGORY.CATEGORY_NAME"));
				item.setNewAndOldId(rs.getInt("ITEM.NEW_AND_OLD_ID"));
				item.setNewAndOldName(rs.getString("NEW_AND_OLD.NEW_AND_OLD_NAME"));
				item.setArtist(rs.getString("ARTIST"));
				item.setPrice(rs.getInt("PRICE"));
				item.setRemarks(rs.getString("REMARKS"));
				PreparedStatement ps2 = connection.prepareStatement(sb2.toString());
				ps2.setInt(1, item.getItem_id());
				ResultSet rs2 = ps2.executeQuery();
				ArrayList<String> genreList = new ArrayList<>();
				while (rs2.next()) {
					genreList.add(rs2.getString("GENRE.GENRE_NAME"));
				}
				item.setGenreName(genreList);
			}
			return item;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}
	
	/**
	 * 検索条件に応じた商品の数を取得する
	 * @param category カテゴリー名
	 * @param genre ジャンル名
	 * @param title　商品名
	 * @return 商品数
	 * @throws SQLRuntimeException　実行時例外
	 */
	public int countSearchItem(String category, String genre, String title) throws SQLRuntimeException {
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        COUNT(DISTINCT ITEM.ITEM_ID) AS CNT");
		sb.append("    FROM");
		sb.append("        ITEM_GENRE");
		sb.append("            LEFT JOIN ITEM");
		sb.append("                ON ITEM_GENRE.ITEM_ID = ITEM.ITEM_ID");
		sb.append("            LEFT JOIN CATEGORY");
		sb.append("                ON ITEM.CATEGORY_ID = CATEGORY.CATEGORY_ID");
		sb.append("            LEFT JOIN GENRE");
		sb.append("                ON GENRE.GENRE_ID = ITEM_GENRE.GENRE_ID");
		sb.append("    WHERE");
		sb.append("        ITEM_NAME LIKE ?");
		sb.append("        AND CATEGORY_NAME = ?");
		sb.append("        AND GENRE_NAME = ?");
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setString(1,  String.format("%1$s%2$s%1$s", "%", title));
			ps.setString(2, category);
			ps.setString(3, genre);
			ResultSet rs = ps.executeQuery();
			int cnt = 0;
			if (rs.next()) {
				cnt = rs.getInt("CNT");
			}
			return cnt;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}
	
	/**
	 * カート内の商品IDからそれぞれの商品の詳細を取得する
	 * @param cartList カート内に入っている商品IDの一覧
	 * @return 商品詳細の一覧
	 * @throws SQLRuntimeException　実行時例外
	 */
	public ArrayList<ItemDTO> selectCartList (ArrayList<Integer> cartList) throws SQLRuntimeException {
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        ITEM_ID");
		sb.append("        ,ITEM_NAME");
		sb.append("        ,ITEM.CATEGORY_ID");
		sb.append("        ,CATEGORY.CATEGORY_NAME");
		sb.append("        ,ITEM.NEW_AND_OLD_ID");
		sb.append("        ,NEW_AND_OLD.NEW_AND_OLD_NAME");
		sb.append("        ,ARTIST");
		sb.append("        ,PRICE");
		sb.append("    FROM");
		sb.append("        ITEM");
		sb.append("            LEFT JOIN CATEGORY");
		sb.append("                ON CATEGORY.CATEGORY_ID = ITEM.CATEGORY_ID");
		sb.append("            LEFT JOIN NEW_AND_OLD");
		sb.append("                ON NEW_AND_OLD.NEW_AND_OLD_ID = ITEM.NEW_AND_OLD_ID");
		sb.append("    WHERE");
		sb.append("        ITEM_ID = ?");
		
		StringBuffer sb2 = new StringBuffer();
		sb2.append("   SELECT");
		sb2.append("          GENRE.GENRE_NAME");
		sb2.append("    FROM ITEM_GENRE");
		sb2.append("    LEFT JOIN GENRE");
		sb2.append("    ON GENRE.GENRE_ID = ITEM_GENRE.GENRE_ID");
		sb2.append("    WHERE ITEM_ID = ?");

		ArrayList<ItemDTO> itemList = new ArrayList<>();
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			for	(int i = 1; i <= cartList.size(); i++) {
				ps.setInt(1, cartList.get(i-1));
				ResultSet rs = ps.executeQuery();
				
				while (rs.next()) {
					ItemDTO item = new ItemDTO();
					item.setItem_id(rs.getInt("ITEM_ID"));
					item.setItemName(rs.getString("ITEM_NAME"));
					item.setCategoryId(rs.getInt("ITEM.CATEGORY_ID"));
					item.setCategoryName(rs.getString("CATEGORY.CATEGORY_NAME"));
					item.setNewAndOldId(rs.getInt("ITEM.NEW_AND_OLD_ID"));
					item.setNewAndOldName(rs.getString("NEW_AND_OLD.NEW_AND_OLD_NAME"));
					item.setArtist(rs.getString("ARTIST"));
					item.setPrice(rs.getInt("PRICE"));
					
					PreparedStatement ps2 = connection.prepareStatement(sb2.toString());
					ps2.setInt(1, item.getItem_id());
					ResultSet rs2 = ps2.executeQuery();
					ArrayList<String> genreList = new ArrayList<>();
					while (rs2.next()) {
						genreList.add(rs2.getString("GENRE.GENRE_NAME"));
					}
					item.setGenreName(genreList);
					
					itemList.add(item);
				}
			}
			return itemList;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}
	
	/**
	 * 入力された商品IDが存在するか確認する
	 * @param itemId　商品ID
	 * @return　真偽値
	 * @throws SQLRuntimeException　実行時例外
	 */
	public boolean isItem (int itemId) throws SQLRuntimeException {
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        COUNT(*) AS CNT");
		sb.append("    FROM");
		sb.append("        ITEM");
		sb.append("    WHERE");
		sb.append("        ITEM_ID = ?");
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setInt(1, itemId);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				if (rs.getInt("CNT") == 1) {
					return true;
				}
			}
			return false;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}
	
}
