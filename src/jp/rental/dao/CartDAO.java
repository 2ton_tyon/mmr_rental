package jp.rental.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import jp.rental.SQLRuntimeException;

public class CartDAO {
	
	private Connection connection;
	
	/**
	 * @param connection
	 */
	public CartDAO (Connection connection) {
		this.connection = connection;
	}
	
	/**
	 * ユーザIDに紐づいたカートテーブルに商品IDを追加する
	 * @param id 商品ID
	 * @param userId ユーザID
	 * @throws SQLRuntimeException 実行時例外
	 */
	public void insertItem (int id, int userId) throws SQLRuntimeException {
		
		StringBuffer sb = new StringBuffer();
		sb.append("INSERT");
		sb.append("    INTO");
		sb.append("        CART(");
		sb.append("            ITEM_ID");
		sb.append("            ,USER_ID");
		sb.append("        )");
		sb.append("    VALUES");
		sb.append("        (");
		sb.append("            ?");
		sb.append("            ,?");
		sb.append("        )");
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setInt(1, id);
			ps.setInt(2, userId);
			ps.executeUpdate();
			
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}

	}
	
	/**
	 * ユーザIDに紐づいたカート内の商品IDの一覧を取得する
	 * @param userId ユーザID
	 * @return ユーザIDに対応するカート内の商品ID一覧
	 * @throws SQLRuntimeException 実行時時例外
	 */
	public ArrayList<Integer> selectItemId (Integer userId) throws SQLRuntimeException {
		
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        ITEM_ID");
		sb.append("    FROM");
		sb.append("        CART");
		sb.append("    WHERE");
		sb.append("        USER_ID = ?");
		
		ArrayList<Integer> idList = new ArrayList<>();
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setInt(1, userId);
			ResultSet rs = ps.executeQuery();
			
			while (rs.next()) {
				idList.add(rs.getInt("ITEM_ID"));
			}	
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return idList;
	}
	
	/**
	 * ユーザIDに紐づいたカート内の商品IDを削除する
	 * @param itemId 商品ID
	 * @param userId ユーザID
	 * @throws SQLRuntimeException 実行時例外
	 */
	public void deleteItem (Integer itemId, Integer userId) throws SQLRuntimeException {
		
		StringBuffer sb = new StringBuffer();
		sb.append("DELETE");
		sb.append("    FROM");
		sb.append("        CART");
		sb.append("    WHERE");
		sb.append("        USER_ID = ?");
		sb.append("        AND ITEM_ID = ? LIMIT 1");

		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setInt(1, userId);
			ps.setInt(2, itemId);
			ps.executeUpdate();		
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}
	
	/**
	 * ユーザIDに紐づくカート内の商品の合計金額を取得する
	 * @param userId ユーザID
	 * @return　カート内のお会計金額
	 * @throws SQLRuntimeException 実行時例外
	 */
	public Integer selectAccounting (Integer userId) throws SQLRuntimeException {

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        SUM(PRICE) AS ACCOUNTING");
		sb.append("    FROM");
		sb.append("        CART");
		sb.append("            LEFT JOIN ITEM");
		sb.append("                ON CART.ITEM_ID = ITEM.ITEM_ID");
		sb.append("    WHERE");
		sb.append("        USER_ID = ?");
		
		Integer accounting = null;
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setInt(1, userId);
			ResultSet rs = ps.executeQuery();
			
			if (rs.next()) {
				accounting = rs.getInt("ACCOUNTING");
			}
			return accounting;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}
	
	/**
	 * ユーザIDに紐づくカート内の商品を全て削除する
	 * @param userId ユーザID
	 * @throws SQLRuntimeException 実行時例外
	 */
	public void deleteAll (Integer userId) throws SQLRuntimeException {
		
		StringBuffer sb = new StringBuffer();
		sb.append("DELETE");
		sb.append("    FROM");
		sb.append("        CART");
		sb.append("    WHERE");
		sb.append("        USER_ID = ?");
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setInt(1, userId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}
}
