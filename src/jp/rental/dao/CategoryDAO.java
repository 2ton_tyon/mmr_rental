package jp.rental.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import jp.rental.SQLRuntimeException;

public class CategoryDAO {
	
	private Connection connection;
	
	/**
	 * コネクションをセットする
	 * @param connection
	 */
	public CategoryDAO(Connection connection) {
		this.connection = connection;
	}
	
	/**
	 * カテゴリーネームの一覧を取得する
	 * @return カテゴリネームの一覧
	 * @throws SQLRuntimeException 実行時例外
	 */
	public ArrayList<String> selectCategory() throws SQLRuntimeException {
		
		// SQL文作成
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        CATEGORY_NAME");
		sb.append("    FROM");
		sb.append("        CATEGORY");
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ResultSet rs = ps.executeQuery();
			
			ArrayList<String> categoryList = new ArrayList<>();
			
			// SQLの結果を取得し、リストに詰める
			while (rs.next()) {
				categoryList.add(rs.getString("CATEGORY_NAME"));
			}
			return categoryList;
			
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} 
	}
	
	/**
	 * カテゴリーに紐づいたジャンルネームの一覧を取得する
	 * @param category カテゴリーネーム
	 * @return ジャンルの一覧
	 * @throws SQLRuntimeException　実行時例外
	 */
	public ArrayList<String> selectGenreByCategory(String category) throws SQLRuntimeException {
		
		// SQL文作成
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        GENRE_NAME");
		sb.append("    FROM");
		sb.append("        GENRE");
		sb.append("    LEFT JOIN");
		sb.append("        CATEGORY");
		sb.append("    ON");
		sb.append("        CATEGORY.CATEGORY_ID = GENRE.CATEGORY_ID");
		sb.append("    WHERE");
		sb.append("        CATEGORY_NAME = ?");
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setString(1, category);
			ResultSet rs = ps.executeQuery();
			
			ArrayList<String> genreList = new ArrayList<>();
			
			// SQLの結果を取得し、リストに詰める
			while (rs.next()) {
				genreList.add(rs.getString("GENRE_NAME"));
			}
			return genreList;
			
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} 
		
	}

}
