package jp.rental.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import jp.rental.SQLRuntimeException;
import jp.rental.dto.CouponDTO;
import jp.rental.dto.ItemDTO;
import jp.rental.dto.OrderHIstoryDTO;
import jp.rental.dto.UserDTO;

public class UserDAO {
	
	private Connection connection;
	
	/**
	 * コネクションをセットする
	 * @param connection
	 */
	public UserDAO(Connection connection) {
		this.connection = connection;
	}
	
	/**
	 * ユーザIDに紐づいた最新の注文履歴から引数で指定した数だけ商品のIDを取得する
	 * @param userId　ユーザID
	 * @param getNum　取得する商品数
	 * @return　商品IDリスト
	 * @throws SQLRuntimeException 実行時例外
	 */
	public ArrayList<ItemDTO> selectOrderItem(Integer userId, int getNum) throws SQLRuntimeException {
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        RENTAL_DETAIL.ITEM_ID");
		sb.append("    FROM");
		sb.append("        RENTAL_DETAIL");
		sb.append("            LEFT JOIN RENTAL");
		sb.append("                ON RENTAL.RENTAL_NUMBER = RENTAL_DETAIL.RENTAL_NUMBER");
		sb.append("    WHERE");
		sb.append("        USER_ID = ?");
		sb.append("    ORDER BY");
		sb.append("        RENTAL.ORDER_DATETIME DESC LIMIT ?");
		
		ArrayList<ItemDTO> itemList = new ArrayList<>();
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setInt(1, userId);
			ps.setInt(2, getNum);
			ResultSet rs = ps.executeQuery();
			
			while (rs.next()) {
				ItemDTO imageData = new ItemDTO();
				
				imageData.setItem_id(rs.getInt("item_id"));
				itemList.add(imageData);
			}
			return itemList;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} 
	}
	
	/**
	 * 引数で与えられた情報に一致したユーザIDを取得する
	 * @param userId　ユーザID
	 * @param password　パスワード
	 * @return　ユーザID
	 * @throws SQLRuntimeException　実行時例外
	 */
	public UserDTO selectUser(String userId, String password) throws SQLRuntimeException {
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        USER_ID");
		sb.append("    FROM");
		sb.append("        USER");
		sb.append("    WHERE");
		sb.append("        MAIL_ADDRESS = ?");
		sb.append("        AND PASS = PASSWORD(?)");

		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setString(1, userId);
			ps.setString(2,password);
			ResultSet rs = ps.executeQuery();
			
			UserDTO userdto = new UserDTO();
			if (rs.next()) {
				userdto.setUserId(rs.getInt("USER_ID"));
			}
			return userdto;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} 
	}
	
	/**
	 * 新規のユーザを追加する
	 * @param userDTO ユーザ情報
	 * @return　追加数
	 * @throws SQLRuntimeException 実行時例外
	 */
	public int insertNewUserData(UserDTO userDTO) throws SQLRuntimeException {
		
		StringBuffer sb = new StringBuffer();
		sb.append("INSERT");
		sb.append("    INTO");
		sb.append("        USER(PASS, MAIL_ADDRESS, USER_NAME, POSTAL_CODE, ADDRESS1, ADDRESS2, TEL, BIRTHDAY, CARD_NUMBER, CARD_EXPIRATION_DATE, CARD_SECURITY_CODE, INVITE_CODE_OF_FRIEND, INVITE_CODE, DELETE_FLG)");
		sb.append("    VALUES");
		sb.append("        (");
		sb.append("            PASSWORD(?)");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,PASSWORD(?)");
		sb.append("            ,0");
		sb.append("        );");
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setString(1, userDTO.getPassword());
			ps.setString(2, userDTO.getMailAddress());
			ps.setString(3, userDTO.getUserName());
			ps.setString(4, userDTO.getPostalCode());
			ps.setString(5, userDTO.getAddress1());
			ps.setString(6, userDTO.getAddress2());
			ps.setString(7, userDTO.getTel());
			ps.setDate(8,userDTO.getBirthdayDate());
			ps.setString(9, userDTO.getCardNumber());
			ps.setString(10, userDTO.getCardExpirationDate());
			ps.setString(11, userDTO.getCardSecurityCode());
			ps.setString(12, userDTO.getInviteCodeFriend());
			ps.setString(13, userDTO.getMailAddress());
		
			return ps.executeUpdate();	
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}
	
	/**
	 * 引数で指定したユーザIDの情報を更新する
	 * @param userId ユーザID
	 * @param userDTO　ユーザ情報
	 * @throws SQLRuntimeException　実行時例外
	 */
	public void updateUserData (Integer userId, UserDTO userDTO) throws SQLRuntimeException {
		
		StringBuffer sb = new StringBuffer();
		sb.append("UPDATE");
		sb.append("        USER");
		sb.append("    SET");
		sb.append("        MAIL_ADDRESS = ?");
		sb.append("        ,USER_NAME = ?");
		sb.append("        ,POSTAL_CODE = ?");
		sb.append("        ,ADDRESS1 = ?");
		sb.append("        ,ADDRESS2 = ?");
		sb.append("        ,TEL = ?");
		sb.append("        ,CARD_NUMBER = ?");
		sb.append("        ,CARD_EXPIRATION_DATE = ?");
		sb.append("        ,CARD_SECURITY_CODE = ?");
		sb.append("    WHERE");
		sb.append("        USER_ID = ?");

		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setString(1, userDTO.getMailAddress());
			ps.setString(2, userDTO.getUserName());
			ps.setString(3, userDTO.getPostalCode());
			ps.setString(4, userDTO.getAddress1());
			ps.setString(5, userDTO.getAddress2());
			ps.setString(6, userDTO.getTel());
			ps.setString(7, userDTO.getCardNumber());
			ps.setString(8, userDTO.getCardExpirationDate());
			ps.setString(9, userDTO.getCardSecurityCode());
			ps.setInt(10, userId);
			ps.executeUpdate();
			
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}
	
	/**
	 * 新規会員登録時の照会は0(ユーザID　0は存在しない)
	 * マイページからの編集時はユーザIDが渡される
	 * @param mail メールアドレス
	 * @param userId ユーザID
	 * @return　真偽値
	 * @throws SQLRuntimeException　実行時例外
	 */
	public boolean selectCheckRegist(String mail, Integer userId) throws SQLRuntimeException {
		
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        COUNT(*) AS CNT");
		sb.append("    FROM");
		sb.append("        USER");
		sb.append("    WHERE");
		sb.append("        MAIL_ADDRESS = ?");
		sb.append("        AND USER_ID != ?");

		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setString(1, mail);
			ps.setInt(2, userId);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				if (rs.getInt("CNT") > 0) {
					return true;
				}
			}
			return false;
		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}
	
	/**
	 * ユーザIDに紐づくクーポンリストを取得する
	 * @param userId ユーザID
	 * @return　クーポンリスト
	 * @throws SQLRuntimeException　実行時例外
	 */
	public ArrayList<CouponDTO> selectCoupon (Integer userId) throws SQLRuntimeException {
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        COUPON.COUPON_ID");
		sb.append("        ,COUPON_CONTENT");
		sb.append("        ,COUPON.DISCOUNT_RATE");
		sb.append("    FROM");
		sb.append("        HAVING_COUPON");
		sb.append("            LEFT JOIN COUPON");
		sb.append("                ON COUPON.COUPON_ID = HAVING_COUPON.COUPON_ID");
		sb.append("    WHERE");
		sb.append("        ACTIVE_FROM > (NOW() - INTERVAL 1 MONTH)");
		sb.append("        AND NOW() >= ACTIVE_UNTIL");
		sb.append("        AND USED_FLG = 0");
		sb.append("        AND USER_ID = ?");
		
		ArrayList<CouponDTO> couponList = new ArrayList<>();
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setInt(1, userId);
			ResultSet rs = ps.executeQuery();
			
			while (rs.next()) {
				CouponDTO couponDTO = new CouponDTO();
				couponDTO.setCouponId(rs.getInt("COUPON.COUPON_ID"));
				couponDTO.setCouponContent(rs.getString("COUPON_CONTENT"));
				couponDTO.setDiscountRate(rs.getInt("COUPON.DISCOUNT_RATE"));
				couponList.add(couponDTO);
			}
			return couponList;
		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}
	
	/**
	 * ユーザIDに紐づくユーザの詳細な情報を取得する
	 * @param userId　ユーザID
	 * @return　ユーザの情報
	 * @throws SQLRuntimeException 実行時例外
	 */
	public UserDTO selectByAllInfo(Integer userId) throws SQLRuntimeException {
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        MAIL_ADDRESS");
		sb.append("        ,USER_NAME");
		sb.append("        ,POSTAL_CODE");
		sb.append("        ,ADDRESS1");
		sb.append("        ,ADDRESS2");
		sb.append("        ,TEL");
		sb.append("        ,BIRTHDAY");
		sb.append("        ,CARD_NUMBER");
		sb.append("        ,CARD_EXPIRATION_DATE");
		sb.append("        ,CARD_SECURITY_CODE");
		sb.append("        ,INVITE_CODE");
		sb.append("    FROM");
		sb.append("        USER");
		sb.append("    WHERE");
		sb.append("        USER.USER_ID = ?");
		
		StringBuffer sb2 = new StringBuffer();
		sb2.append("SELECT");
		sb2.append("        COUPON.COUPON_ID");
		sb2.append("        ,COUPON.COUPON_CONTENT");
		sb2.append("        ,COUPON.ACTIVE_FROM");
		sb2.append("        ,COUPON.ACTIVE_UNTIL");
		sb2.append("    FROM");
		sb2.append("        HAVING_COUPON");
		sb2.append("            LEFT JOIN COUPON");
		sb2.append("                ON HAVING_COUPON.COUPON_ID = COUPON.COUPON_ID");
		sb2.append("    WHERE");
		sb2.append("        HAVING_COUPON.USER_ID = ?");
		sb2.append("        AND HAVING_COUPON.USED_FLG = 0");
		sb2.append("        AND CURDATE() <= ACTIVE_UNTIL");

		
		UserDTO userDTO = new UserDTO();
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setInt(1, userId);
			ResultSet rs = ps.executeQuery();
			
			ArrayList<CouponDTO> couponList = new ArrayList<>();
			
			if (rs.next()) {
				userDTO.setMailAddress(rs.getString("MAIL_ADDRESS"));
				userDTO.setUserName(rs.getString("USER_NAME"));
				userDTO.setPostalCode(rs.getString("POSTAL_CODE"));
				userDTO.setAddress1(rs.getString("ADDRESS1"));
				userDTO.setAddress2(rs.getString("ADDRESS2"));
				userDTO.setTel(rs.getString("TEL"));
				userDTO.setBirthdayDate(rs.getDate("BIRTHDAY"));
				userDTO.setCardNumber(rs.getString("CARD_NUMBER"));
				userDTO.setCardExpirationDate(rs.getString("CARD_EXPIRATION_DATE"));
				userDTO.setCardSecurityCode(rs.getString("CARD_SECURITY_CODE"));
				userDTO.setInviteCode(rs.getString("INVITE_CODE"));
			}	
			
			PreparedStatement ps2 = connection.prepareStatement(sb2.toString());
			ps2.setInt(1, userId);
			ResultSet rs2 = ps2.executeQuery();
			while ( rs2.next()) {
				CouponDTO coupon = new CouponDTO();
				coupon.setCouponId(rs2.getInt("COUPON.COUPON_ID"));
				coupon.setCouponContent(rs2.getString("COUPON_CONTENT"));
				coupon.setActiveFrom(rs2.getDate("COUPON.ACTIVE_FROM"));
				coupon.setActiveUntil(rs2.getDate("COUPON.ACTIVE_UNTIL"));
				couponList.add(coupon);
			}
			userDTO.setCoupons(couponList);
			return userDTO;	
		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}
	
	/**
	 * メールアドレス(一意な値)に一致するユーザIDを取得する
	 * @param mailAddress メールアドレス
	 * @return ユーザID
	 * @throws SQLRuntimeException　実行時例外
	 */
	public Integer selectByMailAddress (String mailAddress) throws SQLRuntimeException {
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        USER_ID");
		sb.append("    FROM");
		sb.append("        USER");
		sb.append("    WHERE");
		sb.append("        MAIL_ADDRESS = ?");
		
		Integer userId = null;
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setString(1, mailAddress);;
			ResultSet rs = ps.executeQuery();
			
			if (rs.next()) {
				userId = rs.getInt("USER_ID");
			}
			return userId;
		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}
	
	/**
	 * ユーザのパスワードを更新する
	 * @param userId　ユーザID
	 * @param password　パスワード
	 * @throws SQLRuntimeException 実行時例外
	 */
	public void updatePassword (Integer userId, String password) throws SQLRuntimeException {
		
		StringBuffer sb = new StringBuffer();
		sb.append("UPDATE");
		sb.append("        USER");
		sb.append("    SET");
		sb.append("        PASS = PASSWORD(?)");
		sb.append("    WHERE");
		sb.append("        USER_ID = ?");

		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setString(1, password);
			ps.setInt(2, userId);
			ps.executeUpdate();
			
		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}
	
	/**
	 * ユーザのパスワードを初期化する
	 * @param userId　ユーザID
	 * @throws SQLRuntimeException　実行時例外
	 */
	public void deletePassword (Integer userId) throws SQLRuntimeException {
		StringBuffer sb = new StringBuffer();
		sb.append("UPDATE");
		sb.append("        USER");
		sb.append("    SET");
		sb.append("        PASS = null");
		sb.append("    WHERE");
		sb.append("        USER_ID = ?");
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setInt(1, userId);
			ps.executeUpdate();
			
		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}
	
	/**
	 * ユーザのパスワードがNullか確認する
	 * @param userId　ユーザID
	 * @return　真偽値
	 * @throws SQLRuntimeException 実行時例外
	 */
	public boolean checkIsNullPassword(Integer userId) throws SQLRuntimeException {
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        PASS");
		sb.append("    FROM");
		sb.append("        USER");
		sb.append("    WHERE");
		sb.append("        USER_ID = ?");
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setInt(1, userId);
			ResultSet rs = ps.executeQuery();
			
			if (rs.next()) {
				if (rs.getString("PASS") == null) {
					return true;
				}
			}
			return false;	
		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}
	
	/**
	 * 詳細な注文履歴に関する情報を取得する
	 * @param userId　ユーザID
	 * @return　注文履歴
	 * @throws SQLRuntimeException　実行時例外
	 */
	public ArrayList<OrderHIstoryDTO> orderHistoryHead(Integer userId) throws SQLRuntimeException {

		StringBuffer sb1 = new StringBuffer();
		sb1.append("SELECT");
		sb1.append("        RENTAL.RENTAL_NUMBER");
		sb1.append("        ,DATE(ORDER_DATETIME) AS ORDER_DATETIME");
		sb1.append("        ,RETURN_DATE");
		sb1.append("        ,STATUS.STATUS_NAME");
		sb1.append("        ,CASE");
		sb1.append("            WHEN RENTAL.USE_COUPON_ID IS NULL THEN SUM(ITEM.PRICE)");
		sb1.append("            ELSE SUM(ITEM.PRICE) * ((100- COUPON.DISCOUNT_RATE) / 100)");
		sb1.append("        END AS FEE");
		sb1.append("    FROM");
		sb1.append("        RENTAL");
		sb1.append("            LEFT JOIN RENTAL_DETAIL");
		sb1.append("                ON RENTAL_DETAIL.RENTAL_NUMBER = RENTAL.RENTAL_NUMBER");
		sb1.append("            LEFT JOIN ITEM");
		sb1.append("                ON RENTAL_DETAIL.ITEM_ID = ITEM.ITEM_ID");
		sb1.append("            LEFT JOIN COUPON");
		sb1.append("                ON COUPON.COUPON_ID = RENTAL.USE_COUPON_ID");
		sb1.append("            LEFT JOIN STATUS");
		sb1.append("                ON STATUS.STATUS_ID = RENTAL.STATUS_ID");
		sb1.append("    WHERE");
		sb1.append("        RENTAL.USER_ID = ?");
		sb1.append("    GROUP BY");
		sb1.append("        RENTAL.RENTAL_NUMBER ORDER BY RENTAL.RENTAL_NUMBER DESC");


		StringBuffer sb2 = new StringBuffer();
		sb2.append("SELECT");
		sb2.append("        RENTAL_DETAIL.ITEM_ID");
		sb2.append("        ,ITEM.ITEM_NAME");
		sb2.append("        ,ITEM.CATEGORY_ID");
		sb2.append("        ,CATEGORY.CATEGORY_NAME");
		sb2.append("        ,ITEM.NEW_AND_OLD_ID");
		sb2.append("        ,NEW_AND_OLD.NEW_AND_OLD_NAME");
		sb2.append("        ,ITEM.ARTIST");
		sb2.append("        ,ITEM.PRICE");
		sb2.append("    FROM");
		sb2.append("        RENTAL_DETAIL");
		sb2.append("            LEFT JOIN ITEM");
		sb2.append("                ON RENTAL_DETAIL.ITEM_ID = ITEM.ITEM_ID");
		sb2.append("            LEFT JOIN NEW_AND_OLD");
		sb2.append("                ON ITEM.NEW_AND_OLD_ID = NEW_AND_OLD.NEW_AND_OLD_ID");
		sb2.append("            LEFT JOIN CATEGORY");
		sb2.append("                ON ITEM.CATEGORY_ID = CATEGORY.CATEGORY_ID");
		sb2.append("    WHERE");
		sb2.append("        RENTAL_NUMBER = ?");
		
		StringBuffer sb3 = new StringBuffer();
		sb3.append("   SELECT");
		sb3.append("          GENRE.GENRE_NAME");
		sb3.append("    FROM ITEM_GENRE");
		sb3.append("    LEFT JOIN GENRE");
		sb3.append("    ON GENRE.GENRE_ID = ITEM_GENRE.GENRE_ID");
		sb3.append("    WHERE ITEM_ID = ?");
		
		Integer itemSum = 0;
		Integer priceSum = 0;

		ArrayList<OrderHIstoryDTO> orderlist = new ArrayList<OrderHIstoryDTO>();
		try{
			PreparedStatement ps = connection.prepareStatement(sb1.toString());
			PreparedStatement ps2 = connection.prepareStatement(sb2.toString());
			PreparedStatement ps3 = connection.prepareStatement(sb3.toString());
			ps.setInt(1, userId);
			ResultSet rs = ps.executeQuery();

			
			while(rs.next()){
				OrderHIstoryDTO order = new OrderHIstoryDTO();
				order.setRental_number(rs.getInt("RENTAL_NUMBER"));
				order.setOrderDate(rs.getDate("order_datetime"));
				order.setReturnDate(rs.getDate("RETURN_DATE"));
				order.setFee(rs.getInt("FEE"));
				orderlist.add(order);
				
				ps2.setInt(1, rs.getInt("rental.rental_number"));
				ResultSet rs2 = ps2.executeQuery();
				ArrayList<ItemDTO> orderDetailList = new ArrayList<>();
				while(rs2.next()){
					ItemDTO orderDetail = new ItemDTO();
					orderDetail.setItem_id(rs2.getInt("rental_detail.item_id"));
					orderDetail.setNewAndOldName(rs2.getString("new_and_old.new_and_old_name"));
					orderDetail.setPrice(rs2.getInt("item.price"));
					orderDetail.setItemName(rs2.getString("item.item_name"));
					orderDetail.setArtist(rs2.getString("item.artist"));
					orderDetail.setCategoryName(rs2.getString("category.category_name"));
					
					itemSum ++;
					priceSum += rs2.getInt("item.price");
					

					
					ps3.setInt(1, rs2.getInt("item_id"));
					ResultSet rs3 = ps3.executeQuery();
					
					ArrayList<String> genreList = new ArrayList<>();
					while(rs3.next()){
						genreList.add(rs3.getString("GENRE.GENRE_NAME"));
						
					}
					orderDetail.setGenreName(genreList);
					orderDetailList.add(orderDetail);
					order.setItemSum(itemSum);
					order.setPriceSum(priceSum);
				}
				order.setItem(orderDetailList);
				itemSum = 0;
				priceSum = 0;
			}
			return orderlist;
		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}
	
	/**
	 * 入力された招待コードをもつユーザが存在するか確認する
	 * @param code　招待コード
	 * @return　クーポン数
	 * @throws SQLRuntimeException 実行時例外
	 */
	public boolean checkFriendCode(String code) throws SQLRuntimeException {
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        COUNT(*) AS CNT");
		sb.append("    FROM");
		sb.append("        USER");
		sb.append("    WHERE");
		sb.append("        INVITE_CODE = ?");
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setString(1, code);
			ResultSet rs = ps.executeQuery();
			
			if (rs.next()) {
				int flg = rs.getInt("CNT");
				if (flg == 1) {
					return true;
				}
			}
			return false;
		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}
	
	/**
	 * 新規追加したユーザにクーポンを付与する
	 * @return　追加数
	 * @throws SQLRuntimeException 実行時例外
	 */
	public int insertCoupon() throws SQLRuntimeException {
		StringBuffer sb = new StringBuffer();
		sb.append("INSERT");
		sb.append("    INTO");
		sb.append("        HAVING_COUPON(");
		sb.append("            COUPON_ID");
		sb.append("            ,USER_ID");
		sb.append("            ,USED_FLG");
		sb.append("        )");
		sb.append("    VALUES");
		sb.append("        (");
		sb.append("            (");
		sb.append("                    SELECT");
		sb.append("                            COUPON_ID");
		sb.append("                        FROM");
		sb.append("                            COUPON");
		sb.append("                        WHERE");
		sb.append("                            CURDATE() >= ACTIVE_FROM");
		sb.append("                            AND CURDATE() <= ACTIVE_UNTIL");
		sb.append("            )");
		sb.append("            ,(");
		sb.append("                SELECT");
		sb.append("                        USER_ID");
		sb.append("                    FROM");
		sb.append("                        USER");
		sb.append("                    ORDER BY");
		sb.append("                        USER_ID DESC LIMIT 1");
		sb.append("            )");
		sb.append("            ,0");
		sb.append("        )");
		
		StringBuffer sb2 = new StringBuffer();
		sb2.append("INSERT");
		sb2.append("    INTO");
		sb2.append("        HAVING_COUPON(");
		sb2.append("            COUPON_ID");
		sb2.append("            ,USER_ID");
		sb2.append("            ,USED_FLG");
		sb2.append("        )");
		sb2.append("    VALUES");
		sb2.append("        (");
		sb2.append("            (");
		sb2.append("                    SELECT");
		sb2.append("                            COUPON_ID");
		sb2.append("                        FROM");
		sb2.append("                            COUPON");
		sb2.append("                        WHERE");
		sb2.append("                            (CURDATE() + INTERVAL 1 MONTH) >= ACTIVE_FROM");
		sb2.append("                            AND (CURDATE() + INTERVAL 1 MONTH) <= ACTIVE_UNTIL");
		sb2.append("            )");
		sb2.append("            ,(");
		sb2.append("                SELECT");
		sb2.append("                        USER_ID");
		sb2.append("                    FROM");
		sb2.append("                        USER");
		sb2.append("                    ORDER BY");
		sb2.append("                        USER_ID DESC LIMIT 1");
		sb2.append("            )");
		sb2.append("            ,0");
		sb2.append("        )");

		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			PreparedStatement ps2 = connection.prepareStatement(sb2.toString()); 
			int a = ps.executeUpdate();
			int b = ps2.executeUpdate();
			System.out.println(a+b);
			if (a == 1 && b == 1) {
				return 1;
			}
			return 0;
		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}
	
	/**
	 * 招待した側にクーポンを配布する
	 * @param friendCode 招待コード
	 * @return 追加数
	 */
	public int insertInviteCoupon(String friendCode) {
		StringBuffer sb = new StringBuffer();
		sb.append("INSERT");
		sb.append("    INTO");
		sb.append("        HAVING_COUPON(");
		sb.append("            COUPON_ID");
		sb.append("            ,USER_ID");
		sb.append("            ,USED_FLG");
		sb.append("        )");
		sb.append("    VALUES");
		sb.append("        (");
		sb.append("            (");
		sb.append("                    SELECT");
		sb.append("                            COUPON_ID");
		sb.append("                        FROM");
		sb.append("                            COUPON");
		sb.append("                        WHERE");
		sb.append("                            CURDATE() >= ACTIVE_FROM");
		sb.append("                            AND CURDATE() <= ACTIVE_UNTIL");
		sb.append("            )");
		sb.append("            ,(");
		sb.append("                SELECT");
		sb.append("                        USER_ID");
		sb.append("                    FROM");
		sb.append("                        USER");
		sb.append("                    WHERE");
		sb.append("                        INVITE_CODE = ?");
		sb.append("            )");
		sb.append("            ,0");
		sb.append("        )");
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setString(1, friendCode);
			return ps.executeUpdate();
		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}

	}
	
	/**
	 * 入力されたクーポンをユーザが持っているか確認する
	 * @param userId　ユーザID
	 * @param couponId　クーポンID
	 * @return 真偽値
	 * @throws SQLRuntimeException　実行時例外
	 */
	public boolean checkHavingCoupon (Integer userId, int couponId) throws SQLRuntimeException {
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        COUNT(*) AS CNT");
		sb.append("    FROM");
		sb.append("        HAVING_COUPON");
		sb.append("    WHERE");
		sb.append("        USER_ID = ?");
		sb.append("        AND COUPON_ID = ?");
		sb.append("        AND USED_FLG = 0");
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setInt(1, userId);
			ps.setInt(2, couponId);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				if (rs.getInt("CNT") > 0) {
					return true;
				}
			}
			return false;
		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}
	
	/**
	 * @param userId
	 * @return
	 * @throws SQLRuntimeException　実行時例外
	 */
	public boolean checkCart(String mail) throws SQLRuntimeException {
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("	        COUNT(*) AS CNT");
		sb.append("	    FROM");
		sb.append("	        CART");
		sb.append("	            LEFT JOIN USER");
		sb.append("	                ON CART.USER_ID = USER.USER_ID");
		sb.append("	    WHERE");
		sb.append("	        USER.MAIL_ADDRESS = ?");

		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setString(1, mail);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				if (rs.getInt("CNT") > 0) {
					return true;
				}
			}
			return false;
		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}
	
	public boolean checkCart(Integer userId) throws SQLRuntimeException {
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("	        COUNT(*) AS CNT");
		sb.append("	    FROM");
		sb.append("	        CART");
		sb.append("	    WHERE");
		sb.append("	        USER_ID = ?");

		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setInt(1, userId);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				if (rs.getInt("CNT") > 0) {
					return true;
				}
			}
			return false;
		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}
}