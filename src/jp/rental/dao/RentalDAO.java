package jp.rental.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import jp.rental.SQLRuntimeException;
import jp.rental.dto.RentalDTO;

public class RentalDAO {
	
	private Connection connection;
	
	/**
	 * コネクションをセットする
	 * @param connection
	 */
	public RentalDAO (Connection connection) {
		this.connection = connection;
	}
	
	
	/**
	 * レンタルに関する情報をDBに追加する
	 * @param rentalDetail レンタルの詳細
	 * @throws SQLRuntimeException 実行時例外
	 */
	public void insertRental (RentalDTO rentalDetail) throws SQLRuntimeException {
		
		StringBuffer sb = new StringBuffer();
		sb.append("INSERT");
		sb.append("    INTO");
		sb.append("        RENTAL(");
		sb.append("            USER_ID");
		sb.append("            ,USE_COUPON_ID");
		sb.append("            ,ORDER_DATETIME");
		sb.append("            ,RETURN_DATE_PLAN");
		sb.append("            ,DELIVERY_DATE");
		sb.append("            ,DELIVERY_TIME_ID");
		sb.append("            ,STATUS_ID");
		sb.append("        )");
		sb.append("    VALUES");
		sb.append("        (");
		sb.append("            ?");
		sb.append("            ,?");
		sb.append("            ,NOW()");
		sb.append("            ,DATE_ADD(? , INTERVAL 8 DAY)");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,1");
		sb.append("        )");
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			
			ps.setInt(1, rentalDetail.getUserId());
			if (rentalDetail.getUseCouponId() == null) {
				ps.setNull(2,  java.sql.Types.INTEGER);
			} else {
				ps.setInt(2, rentalDetail.getUseCouponId());
			}
			ps.setDate(3, rentalDetail.getDeliveryDate());
			ps.setDate(4, rentalDetail.getDeliveryDate());
			ps.setInt(5, rentalDetail.getDeliveryTimeId());
			ps.executeUpdate();
			
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}
	
	/**
	 * ユーザIDに紐づいた最新のレンタルNoを取得する
	 * @param userId ユーザID
	 * @return　レンタルNo
	 * @throws SQLRuntimeException　実行時例外
	 */
	public Integer selectRentalId (Integer userId) throws SQLRuntimeException {
		
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        RENTAL_NUMBER");
		sb.append("    FROM");
		sb.append("        RENTAL");
		sb.append("    WHERE");
		sb.append("        USER_ID = ?");
		sb.append("    ORDER BY");
		sb.append("        RENTAL_NUMBER DESC LIMIT 1");
		
		Integer id = null;
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setInt(1, userId);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				id = rs.getInt("RENTAL_NUMBER");
			}
			return id;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}
	
	/**
	 * レンタルした商品のIDをレンタル詳細に追加する
	 * @param rentalNumber レンタルNo
	 * @param itemList　商品リスト
	 * @throws SQLRuntimeException　実行時例外
	 */
	public void insertRentalDetail (Integer rentalNumber, ArrayList<Integer> itemList) throws SQLRuntimeException {
		StringBuffer sb = new StringBuffer();
		sb.append("INSERT");
		sb.append("    INTO");
		sb.append("        RENTAL_DETAIL(");
		sb.append("            RENTAL_NUMBER");
		sb.append("            ,ITEM_ID");
		sb.append("        )");
		sb.append("    VALUES");
		sb.append("        (");
		sb.append("            ?");
		sb.append("            ,?");
		sb.append("        )");
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setInt(1, rentalNumber);
			for (Integer itemId : itemList) {
				ps.setInt(2, itemId);
				ps.executeUpdate();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}
	
	/**
	 * レンタル時に使用したクーポンIDを使用済みに更新する
	 * @param userId　ユーザID
	 * @param couponId　クーポンID
	 * @throws SQLRuntimeException 実行時例外
	 */
	public void updateCouponUsedFlag (Integer userId, Integer couponId) throws SQLRuntimeException {
		StringBuffer sb = new StringBuffer();
		sb.append("UPDATE");
		sb.append("        HAVING_COUPON");
		sb.append("    SET");
		sb.append("        USED_FLG = 1");
		sb.append("    WHERE");
		sb.append("        USER_ID = ?");
		sb.append("        AND USED_FLG = 0 ");
		sb.append("        AND COUPON_ID = ? LIMIT 1");
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setInt(1, userId);
			ps.setInt(2, couponId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}
}
