package jp.rental.user;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.rental.DataSourceManager;
import jp.rental.SQLRuntimeException;
import jp.rental.Validate;
import jp.rental.dao.CartDAO;
import jp.rental.dao.RentalDAO;
import jp.rental.dao.UserDAO;
import jp.rental.dto.RentalDTO;

/**
 * Servlet implementation class OrderConfirmServlet
 */
@WebServlet("/rental")
public class OrderConfirmServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("top");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if(session.getAttribute("userId") == null) {
			//　システムの不具合で未ログインユーザがレンタル処理の画面に来ることができた場合を防ぐ
			response.sendRedirect("login");
			return;
		}
		
		// 希望の配達日時を取得することができる
		String deliveryDate = request.getParameter("deliveryDate");
		// DELIVERY_DATE_IDを取得することができる
		String receiveTime = request.getParameter("time");
		// クーポンIDを取得することができる
		String coupon = request.getParameter("coupon");
		Integer userId = (Integer) session.getAttribute("userId");
		
		
		RentalDTO rentalDetail = new RentalDTO();
		rentalDetail.setUserId(userId);
		try (Connection connection = DataSourceManager.getConnection()){
			UserDAO userDAO = new UserDAO(connection);
			// 他の端末で会計済み
			if (!userDAO.checkCart(userId)) {
				session.setAttribute("errorMessage", "お会計時に不具合が発生しました。");
				response.sendRedirect("cart");
				return;
			}
			
			boolean couponFlag = false;
			// coupon情報を格納する
			if (Validate.isStrParseInt(coupon) && userDAO.checkHavingCoupon(userId,Integer.parseInt(coupon))) {
				couponFlag = true;
				rentalDetail.setUseCouponId(Integer.parseInt(coupon));
			} else {
				rentalDetail.setUseCouponId(null);
			}
			
			// 希望受け取り時間を格納する
			ArrayList<String> receiveList = new ArrayList<>(Arrays.asList("1", "2", "3","4"));
			if (!Validate.isStrParseInt(receiveTime) && receiveList.contains(receiveTime)) {
				// requiredな要素が渡されていない時
				session.setAttribute("errorMessage", "入力情報に不備があったため、はじめから手続きをお願いします。");
				response.sendRedirect("cart");
				return;
			} else {
				rentalDetail.setDeliveryTimeId(Integer.parseInt(receiveTime));
			}
			
			// 希望配達日時を格納する
			if (!Validate.isStrParseDate(deliveryDate)) {
				// requiredな要素が渡されていない時
				session.setAttribute("errorMessage", "入力情報に不備があったため、はじめから手続きをお願いします。");
				response.sendRedirect("cart");
				return;
			} else {
				Calendar cal = Calendar.getInstance();
				Calendar today = Calendar.getInstance();
				Calendar returnDay = Calendar.getInstance();
				returnDay.add(Calendar.DAY_OF_MONTH, 8);
				String[] _date = deliveryDate.split("-");
				// 入力された日付をカレンダー型に変換
				try {
					cal.set(Integer.parseInt(_date[0]),Integer.parseInt(_date[1])-1,Integer.parseInt(_date[2]));
				} catch (NumberFormatException | NullPointerException e) {
					session.setAttribute("errorMessage", "入力情報に不備があったため、はじめから手続きをお願いします。");
					response.sendRedirect("cart");
					return;
				}
				// 選択した日付が正しい日付か確認する
				if (!cal.after(today) || !cal.before(returnDay)) {
					session.setAttribute("errorMessage", "入力情報に不備があったため、はじめから手続きをお願いします。");
					response.sendRedirect("cart");
					return;
				}
				
		        Date date = Date.valueOf(deliveryDate);
		        rentalDetail.setDeliveryDate(date);
			}
			
			connection.setAutoCommit(false);
			
			// 1　レンタルの処理を行う
			RentalDAO rental = new RentalDAO(connection);
			rental.insertRental(rentalDetail);
			
			// 2　注文番号の取得を行う
			Integer rentalId = rental.selectRentalId(userId);
			request.setAttribute("id", rentalId);
			
			// 3 クーポンが使われていればUSER_FLGを立てる
			
			if (couponFlag) {
				rental.updateCouponUsedFlag(userId, rentalDetail.getUseCouponId());
			}
			
			// 4 レンタル詳細にレンタルした商品を登録する
			CartDAO cartDAO = new CartDAO(connection);
			ArrayList<Integer> itemList = cartDAO.selectItemId(userId);
			rental.insertRentalDetail(rentalId, itemList);
			
			// 5 カート内の商品をすべて削除する
			cartDAO.deleteAll(userId);
			
			session.removeAttribute("cart");
			
			connection.commit();
			
			request.getRequestDispatcher("WEB-INF/jsp/orderComplete.jsp").forward(request, response);
		} catch (SQLException | SQLRuntimeException e) {
			request.getRequestDispatcher("WEB-INF/jsp/systemError.jsp").forward(request, response);
		}
	}
}
