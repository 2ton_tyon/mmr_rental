package jp.rental.user;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.rental.DataSourceManager;
import jp.rental.SQLRuntimeException;
import jp.rental.Validate;
import jp.rental.dao.CategoryDAO;
import jp.rental.dao.UserDAO;
/**
 * Servlet implementation class ReRegistPasswordServlet
 */
@WebServlet("/reregistPassword")
public class ReRegistPasswordServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		
		// ログインしているユーザはアクセスできない
		if (session.getAttribute("userId") != null) {
			response.sendRedirect("top");
			return;
		}
		
		try (Connection connection = DataSourceManager.getConnection()) {
			
			if (session.getAttribute("category") == null) {
				CategoryDAO categoryDAO = new CategoryDAO(connection);
				ArrayList<String> categoryList = categoryDAO.selectCategory();
				HashMap<String, ArrayList<String>> list = new HashMap<>();
				for(String category : categoryList) {
					list.put(category, categoryDAO.selectGenreByCategory(category));
				}
				session.setAttribute("category", categoryList);
				session.setAttribute("allGenreList", list);
			}
				
			String query = request.getParameter("code");
			
			// 初回アクセス時はそのまま表示
			if (query == null || "".equals(query) ) {
				request.getRequestDispatcher("WEB-INF/jsp/reRegist.jsp").forward(request, response);
				return;
			}
			
			// 入力されたコードが数字ではない
			if (!Validate.isStrParseInt(query)) {
				request.setAttribute("errorMessage", "認証に失敗しました。再度メールを送信してください");
				request.getRequestDispatcher("WEB-INF/jsp/reRegist.jsp").forward(request, response);
				return;
			}
			
			UserDAO userDAO = new UserDAO(connection);
			if (!userDAO.checkIsNullPassword(Integer.parseInt(query))) {
				request.setAttribute("errorMessage", "認証に失敗しました。再度メールを送信してください");
				request.getRequestDispatcher("WEB-INF/jsp/reRegist.jsp").forward(request, response);
				return;
			}
			request.setAttribute("id", query);
			request.getRequestDispatcher("WEB-INF/jsp/registPassword.jsp").forward(request, response);
			
		} catch (SQLException | SQLRuntimeException e) {
			// DBに接続できない時
			request.getRequestDispatcher("WEB-INF/jsp/systemError.jsp").forward(request, response);
		}
	}
	
	/**
	 * @return 
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		
		String userId = request.getParameter("id");
		String password = request.getParameter("password");
		String confirmPassword = request.getParameter("confirmPassword");
		
		if (session.getAttribute("userId") != null) {
			response.sendRedirect("top");
			return;
		}
		
		// 送信されたIDが数値か確認する
		// 入力されたパスワードと確認用パスワードが同一のものか確認する
		if (!Validate.isStrParseInt(userId) ){
			response.sendRedirect("top");
			return;
		}
		
		if (!Validate.isEmpty(password) && !password.equals(confirmPassword)) {
			request.setAttribute("errorMessage", "入力されたパスワードと確認用パスワードが異なります。");
			request.getRequestDispatcher("WEB-INF/jsp/registPassword.jsp").forward(request, response);
			return;
		}
		
		if (!Validate.isRgularExpression(password, "[a-zA-Z0-9]{8,41}")) {
			request.setAttribute("errorMessage", "入力されたパスワードと確認用パスワードが異なります。");
			request.getRequestDispatcher("WEB-INF/jsp/registPassword.jsp").forward(request, response);
			return;
		}
		
		try (Connection connection = DataSourceManager.getConnection()) {
			UserDAO userDAO = new UserDAO(connection);
			if (userDAO.checkIsNullPassword(Integer.parseInt(userId))) {
				userDAO.updatePassword(Integer.parseInt(userId),password);
				response.sendRedirect("login");
				return;
			}
		} catch (SQLException |SQLRuntimeException e) {
			request.getRequestDispatcher("WEB-INF/jsp/systemError.jsp").forward(request, response);
		}	
	}
}
