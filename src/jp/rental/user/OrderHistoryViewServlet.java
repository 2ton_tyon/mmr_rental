package jp.rental.user;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.rental.DataSourceManager;
import jp.rental.SQLRuntimeException;
import jp.rental.dao.UserDAO;
import jp.rental.dto.OrderHIstoryDTO;

/**
 * Servlet implementation class OrderHistoryViewServlet
 */
@WebServlet("/orderHistory")
public class OrderHistoryViewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		if(session.getAttribute("userId") == null){
			response.sendRedirect("login");
			return;
		}
		
		try(Connection connection = DataSourceManager.getConnection()){
			Integer userId = (Integer) session.getAttribute("userId");
			UserDAO userDao = new UserDAO(connection);
			
			ArrayList<OrderHIstoryDTO> list = userDao.orderHistoryHead(userId);
			System.out.println("");
			request.setAttribute("oldorderstatus", list);
			request.getRequestDispatcher("WEB-INF/jsp/orderHistory.jsp").forward(request, response);	
			
		} catch (SQLException | SQLRuntimeException e) {
			request.getRequestDispatcher("WEB-INF/jsp/systemError.jsp").forward(request, response);
		}
	}

}
