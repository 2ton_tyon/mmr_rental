package jp.rental.user;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.rental.DataSourceManager;
import jp.rental.SQLRuntimeException;
import jp.rental.dao.CategoryDAO;
import jp.rental.dao.ItemDAO;
import jp.rental.dao.UserDAO;
import jp.rental.dto.ItemDTO;
/**
 * Servlet implementation class HomeServlet
 */
@WebServlet("/top")
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// セッションを取得する
		
		HttpSession session = request.getSession();
		
		try (Connection connection = DataSourceManager.getConnection()) {
			
			if (session.getAttribute("category") == null) {
				CategoryDAO categoryDAO = new CategoryDAO(connection);
				ArrayList<String> categoryList = categoryDAO.selectCategory();
				HashMap<String, ArrayList<String>> list = new HashMap<>();
				for(String category : categoryList) {
					list.put(category, categoryDAO.selectGenreByCategory(category));
				}
				session.setAttribute("category", categoryList);
				session.setAttribute("allGenreList", list);
			}
			
			if (session.getAttribute("userId") != null) {
				Integer userId = (Integer) session.getAttribute("userId");
				ItemDAO itemDAO = new ItemDAO(connection);
				UserDAO userDAO = new UserDAO(connection);
				int getNum = 5;
				ArrayList<ItemDTO> itemList = itemDAO.selectRecommendItem(getNum);
				ArrayList<ItemDTO> orderList = userDAO.selectOrderItem(userId,getNum);
			    request.setAttribute("itemList",itemList);
			    request.setAttribute("orderList", orderList);
			} else {
				ItemDAO itemDAO = new ItemDAO(connection);
				int getNum = 10;
				ArrayList<ItemDTO> itemList = itemDAO.selectRecommendItem(getNum);
			    request.setAttribute("itemList",itemList);
			}
			request.getRequestDispatcher("WEB-INF/jsp/top.jsp").forward(request, response);

		} catch (SQLException | SQLRuntimeException e) {
			request.getRequestDispatcher("WEB-INF/jsp/systemError.jsp").forward(request, response);
		}
	}
}
