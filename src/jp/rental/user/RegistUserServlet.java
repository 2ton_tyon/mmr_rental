package jp.rental.user;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.rental.DataSourceManager;
import jp.rental.SQLRuntimeException;
import jp.rental.Validate;
import jp.rental.dao.CategoryDAO;
import jp.rental.dao.UserDAO;
import jp.rental.dto.UserDTO;

/**
 * Servlet implementation class RegistUserServlet
 */
@WebServlet("/regist")
public class RegistUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("userId") != null) {
			response.sendRedirect("top");
			return;
		}
		try (Connection connection = DataSourceManager.getConnection()) {		
			if (session.getAttribute("category") == null) {
				CategoryDAO categoryDAO = new CategoryDAO(connection);
				ArrayList<String> categoryList = categoryDAO.selectCategory();
				HashMap<String, ArrayList<String>> list = new HashMap<>();
				for(String category : categoryList) {
					list.put(category, categoryDAO.selectGenreByCategory(category));
				}
				session.setAttribute("category", categoryList);
				session.setAttribute("allGenreList", list);
			}
		} catch (SQLException | SQLRuntimeException e) {
			// DBとの接続に失敗
			request.getRequestDispatcher("WEB-INF/jsp/systemError.jsp").forward(request, response);
			return;
		}
		request.getRequestDispatcher("WEB-INF/jsp/registNewMember.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("utf-8");
		
		String name = request.getParameter("name");
		String mail = request.getParameter("email");
		String mailConfirm = request.getParameter("confirmEmail");
		String TEL = request.getParameter("tel");
		String post = request.getParameter("zip");
		String addr1 = request.getParameter("address");
		String addr2 = request.getParameter("address2");
		String birthday = request.getParameter("birthday");
		String cardNo = request.getParameter("cardNo");
		String limit = request.getParameter("limit");
		String code = request.getParameter("code");
		String password = request.getParameter("password");
		String confirmPassword = request.getParameter("confirmPassword");
		String friendCode = request.getParameter("friendCode");
		
		// エラーメッセージ
		ArrayList<String> errorMsg = new ArrayList<>();
		
		UserDTO userDTO = new UserDTO();
		
		
		// 名前 40文字以内
		if (Validate.isEmpty(name)) {
			errorMsg.add("名前を40文字以内で入力してください。");
		} else if (name.length() > 40){
			errorMsg.add("名前を40文字以内で入力してください。");
		}
		userDTO.setUserName(name);
		
		
		if (!Validate.isEmpty(mail) && !mail.equals(mailConfirm)) {
			errorMsg.add("入力されたメールアドレスと確認用のメールアドレスが異なります。");
		}
		userDTO.setMailAddress(mail);
		
		// メール　50文字以内
		if (!Validate.isRgularExpression(mail,"^[a-zA-Z0-9_]{1,30}@[a-zA-Z0-9.]{1,19}")) {
			errorMsg.add("メールアドレスを50文字以内で入力してください。");
		}
		
		// TEL 10 - 11
		if (!Validate.isRgularExpression(TEL, "0{1}[0-9]{9,10}")) {
			errorMsg.add("電話番号を11文字以内で入力してください。");
		};
		userDTO.setTel(TEL);
		
		// 郵便番号
		if (!Validate.isRgularExpression(post, "^[0-9]{7}$")) {
			errorMsg.add("郵便番号をハイフンなし半角数字で入力してください。");
		}
		userDTO.setPostalCode(post);
		
		// 住所
		if (Validate.isEmpty(addr1) || Validate.isEmpty(addr2)) {
			errorMsg.add("住所1、住所2を50文字以内で入力してください。");
		} else if ( addr1.length() > 50 || addr2.length() > 50) {
			errorMsg.add("住所1、住所2を50文字以内で入力してください。");
		}
		userDTO.setAddress1(addr1);
		userDTO.setAddress2(addr2);
		
		// 誕生日
		if (!Validate.isStrParseDate(birthday)) {
			errorMsg.add("誕生日を入力してください。");
		} else {
			
			Calendar cal = Calendar.getInstance();
			Calendar checkDate = Calendar.getInstance();
			// 18年前の同じ日付に設定
			checkDate.add(Calendar.YEAR, -18);
			String[] _date = birthday.split("-");
			// 入力された日付をカレンダー型に変換
			try {
				cal.set(Integer.parseInt(_date[0]),Integer.parseInt(_date[1])-1,Integer.parseInt(_date[2]));
				// 選択した日付が正しい日付か確認する
				if (!checkDate.after(cal) ) {
					errorMsg.add("未成年の方は登録することができません。");
				}
				userDTO.setBirthdayDate(Date.valueOf(birthday));
			} catch (NumberFormatException | NullPointerException e) {
				errorMsg.add("誕生日を入力してください。");
			}
		}
		
		// クレジットカード
		if (!Validate.isRgularExpression(cardNo, "[0-9]{16}")) {
			errorMsg.add("クレジットカード番号を16桁で入力してください。");
		}
		userDTO.setCardNumber(cardNo);
		
		// 有効期限
		if (!Validate.isRgularExpression(limit, "[0-9]{4}")) {
			errorMsg.add("有効期限を4桁で入力してください。");
		}
		userDTO.setCardExpirationDate(limit);
		
		// セキュリティコード
		if (!Validate.isRgularExpression(code, "[0-9]{3,4}")) {
			errorMsg.add("セキュリティコードを入力してください。");
		}
		userDTO.setCardSecurityCode(code);
		
		if (!password.equals(confirmPassword)) {
			errorMsg.add("パスワードと確認用パスワードが異なります。");
		}
		
		if (!Validate.isRgularExpression(password, "[a-zA-Z0-9]{8,41}")) {
			errorMsg.add("パスワードを8文字以上41字以内の半角英数字で入力してください。");
		}
		userDTO.setPassword(password);
		
		userDTO.setInviteCodeFriend(friendCode);
		
		if (errorMsg.size() > 0) {
			request.setAttribute("user", userDTO);
			request.setAttribute("errorMsg", errorMsg);
			request.getRequestDispatcher("WEB-INF/jsp/registNewMember.jsp").forward(request, response);
			return;
		}
		
		try (Connection connection = DataSourceManager.getConnection()) {
			UserDAO userDAO = new UserDAO(connection);
			// 入力されたメールアドレスがすでに利用されているか確認する
			if (userDAO.selectCheckRegist(userDTO.getMailAddress(),0)){
				request.setAttribute("user", userDTO);
				request.setAttribute("errorMessage", "既に登録済みのメールアドレスです");
				request.getRequestDispatcher("WEB-INF/jsp/registNewMember.jsp").forward(request, response);
				return;
			}
			//　招待クーポンが入力されている場合,DBの招待クーポンとの整合性を確認する
			boolean inviteFlg = false;
			if (friendCode != null && !"".equals(friendCode)){
				if (userDAO.checkFriendCode(friendCode)) {
					inviteFlg = true;
				} else {
					request.setAttribute("user", userDTO);
					request.setAttribute("errorMessage", "入力した招待コードは利用できません");
					request.getRequestDispatcher("WEB-INF/jsp/registNewMember.jsp").forward(request, response);
					return;
				}
			}
			// 自動コミットを解除
			connection.setAutoCommit(false);
			// ユーザ情報の登録
			if (userDAO.insertNewUserData(userDTO) != 1){
				connection.rollback();
				request.setAttribute("errorMessage", "ユーザ登録に失敗しました");
				request.getRequestDispatcher("WEB-INF/jsp/registNewMember.jsp").forward(request, response);
				return;
			}
			if (inviteFlg) {
				// 招待クーポンを配布する
				if (userDAO.insertCoupon() != 1 || userDAO.insertInviteCoupon(friendCode) != 1){
					connection.rollback();
					request.setAttribute("errorMessage", "ユーザ登録に失敗しました");
					request.getRequestDispatcher("WEB-INF/jsp/registNewMember.jsp").forward(request, response);
					return;
				}
			}
			connection.commit();
			HttpSession session = request.getSession();
			session.setAttribute("msg", "登録が完了しました。");
			response.sendRedirect("login");
			
		} catch (SQLException | SQLRuntimeException e) {
			e.printStackTrace();
			request.getRequestDispatcher("WEB-INF/jsp/systemError.jsp").forward(request, response);
		}
		
		
	}

}
