package jp.rental.user;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.rental.DataSourceManager;
import jp.rental.SQLRuntimeException;
import jp.rental.Validate;
import jp.rental.dao.CategoryDAO;
import jp.rental.dao.ItemDAO;
import jp.rental.dto.ItemDTO;

/**
 * Servlet implementation class ItemDetailServlet
 */
@WebServlet("/detail")
public class ItemDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String searchId = request.getParameter("id");
		try (Connection connection = DataSourceManager.getConnection()) {
			
			if (session.getAttribute("category") == null) {
				CategoryDAO categoryDAO = new CategoryDAO(connection);
				ArrayList<String> categoryList = categoryDAO.selectCategory();
				HashMap<String, ArrayList<String>> list = new HashMap<>();
				for(String category : categoryList) {
					list.put(category, categoryDAO.selectGenreByCategory(category));
				}
				session.setAttribute("category", categoryList);
				session.setAttribute("allGenreList", list);
			}
			
			ItemDAO itemDAO = new ItemDAO(connection);

			if (!Validate.isStrParseInt(searchId)) {
				request.getRequestDispatcher("WEB-INF/jsp/systemError.jsp").forward(request, response);
				return;
			}
			if (!itemDAO.isItem(Integer.parseInt(searchId))) {
				// 存在しないItemIDが入力された
				request.getRequestDispatcher("WEB-INF/jsp/systemError.jsp").forward(request, response);
				return;
			}
			
			//作品の詳細を取得する
			ItemDTO itemDTO = itemDAO.selectItemDetail(Integer.parseInt(searchId));
			request.setAttribute("item", itemDTO);
			request.getRequestDispatcher("WEB-INF/jsp/detail.jsp").forward(request, response);
			
		} catch (SQLException |SQLRuntimeException e) {
			request.getRequestDispatcher("WEB-INF/jsp/systemError.jsp").forward(request, response);
		}
		
	}

}
