package jp.rental.user;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.rental.DataSourceManager;
import jp.rental.SQLRuntimeException;
import jp.rental.Validate;
import jp.rental.dao.UserDAO;
import jp.rental.dto.UserDTO;

/**
 * Servlet implementation class UpdateUserInfoServlet
 */
@WebServlet("/updateUser")
public class UpdateUserInfoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("top");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		// ログインしていないユーザはアクセスできない
		if (session.getAttribute("userId") == null) {
			response.sendRedirect("top");
			return;
		}
		
		Integer userId = (Integer) session.getAttribute("userId");
		
		request.setCharacterEncoding("utf-8");
		
		String name = request.getParameter("name");
		String mail = request.getParameter("email");
		String mailConfirm = request.getParameter("confirmEmail");
		String TEL = request.getParameter("tel");
		String post = request.getParameter("zip");
		String addr1 = request.getParameter("address");
		String addr2 = request.getParameter("address2");
		String cardNo = request.getParameter("cardNo");
		String limit = request.getParameter("limit");
		String code = request.getParameter("code");
		String password = request.getParameter("password");
		String confirmPassword = request.getParameter("confirmPassword");
		
		// エラーメッセージ
		ArrayList<String> errorMsg = new ArrayList<>();
		
		// 名前 40文字以内
		if (Validate.isEmpty(name)) {
			errorMsg.add("名前を40文字以内で入力してください。");
		} else if (name.length() > 40){
			errorMsg.add("名前を40文字以内で入力してください。");
		}
		
		if (!Validate.isEmpty(mail) && !mail.equals(mailConfirm)) {
			errorMsg.add("入力されたメールアドレスと確認用のメールアドレスが異なります。");
		}
		
		// メール　50文字以内
		if (!Validate.isRgularExpression(mail,"^[a-zA-Z0-9_]{1,30}@[a-zA-Z0-9.]{1,19}")) {
			errorMsg.add("メールアドレスを50文字以内で入力してください。\n");
		}
		
		// TEL 10 - 11
		if (!Validate.isRgularExpression(TEL, "0{1}[0-9]{9,10}")) {
			errorMsg.add("電話番号を11文字以内で入力してください。");
		};
		// 郵便番号
		if (!Validate.isRgularExpression(post, "^[0-9]{7}$")) {
			errorMsg.add("郵便番号ををハイフンなし半角数字で入力してください。");
		}
		
		// 住所
		if (Validate.isEmpty(addr1) || Validate.isEmpty(addr2)) {
			errorMsg.add("住所1、住所2を50文字以内で入力してください。");
		} else if ( addr1.length() > 50 || addr2.length() > 50) {
			errorMsg.add("住所1、住所2を50文字以内で入力してください。");
		}
		
		// クレジットカード
		if (!Validate.isRgularExpression(cardNo, "[0-9]{16}")) {
			errorMsg.add("クレジットカード番号を16桁で入力してください。");
		}
		// 有効期限
		if (!Validate.isRgularExpression(limit, "[0-9]{4}")) {
			errorMsg.add("有効期限を4桁で入力してください。");
		}
		
		// セキュリティコード
		if (!Validate.isRgularExpression(code, "[0-9]{3,4}")) {
			errorMsg.add("セキュリティコードを入力してください。");
		}
		
		if (!Validate.isEmpty(password) && !password.equals(confirmPassword)) {
			errorMsg.add("パスワードと確認用パスワードが異なります。");
		}
		
		if (!Validate.isEmpty(password) && !Validate.isRgularExpression(password, "[a-zA-Z0-9]{8,41}")) {
			errorMsg.add("パスワードを8文字以上41字以内の半角英数字で入力してください。");
		}
		// 入力必須項目が適切な形で入力されていることを確認する
		
		if (errorMsg.size() > 0) {
			session.setAttribute("errorMsg", errorMsg);
			response.sendRedirect("user_page");
			return;
		}
		
		UserDTO userDTO = new UserDTO();
		userDTO.setUserName(name);
		userDTO.setMailAddress(mail);
		userDTO.setTel(TEL);
		userDTO.setPostalCode(post);
		userDTO.setAddress1(addr1);
		userDTO.setAddress2(addr2);
		userDTO.setCardNumber(cardNo);
		userDTO.setCardExpirationDate(limit);
		userDTO.setCardSecurityCode(code);
		userDTO.setPassword(password);
		
		try (Connection connection = DataSourceManager.getConnection()) {
			UserDAO userDAO = new UserDAO(connection);
			if (!userDAO.selectCheckRegist(userDTO.getMailAddress(),userId)){
				connection.setAutoCommit(false);
				// ユーザ情報の更新を行う
				userDAO.updateUserData(userId,userDTO);
				// パスワードの変更希望があれば更新する
				if (!Validate.isEmpty(password)) {
					userDAO.updatePassword(userId, password);
				}
				connection.commit();
				session.setAttribute("msg", "更新が完了しました");
				response.sendRedirect("user_page");
			} else {
				errorMsg.add("既に登録済みのメールアドレスです");
				session.setAttribute("errorMsg", errorMsg);
				response.sendRedirect("user_page");
			}
			
		} catch (SQLException |SQLRuntimeException e) {
			request.getRequestDispatcher("WEB-INF/jsp/systemError.jsp").forward(request, response);
		}
	}

}
