package jp.rental.user;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.rental.DataSourceManager;
import jp.rental.SQLRuntimeException;
import jp.rental.dao.CartDAO;
import jp.rental.dao.UserDAO;
import jp.rental.dto.CouponDTO;

/**
 * Servlet implementation class ProceedRentalServlet
 */
@WebServlet("/proceed")
public class ProceedRentalServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("top");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 * ログインしているユーザは送信されたアイテムとDBに格納されているアイテムのリストを照合する
	 * 問題なければ会計金額を取得してお届け日時画面に遷移
	 * ログインしていないユーザはログイン画面に遷移させる
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		if(session.getAttribute("userId") == null) {
			//　ログインしていないユーザ
			request.getRequestDispatcher("WEB-INF/jsp/login.jsp").forward(request, response);
		} else {
			String[] itemList = request.getParameterValues("itemId");
			ArrayList<Integer> cart  = new ArrayList<>();
			for (String item: itemList) {
				cart.add(Integer.parseInt(item));
			}
			
			try (Connection connection = DataSourceManager.getConnection()) {
				CartDAO cartDAO = new CartDAO(connection);
				UserDAO userDAO = new UserDAO(connection);
				// 他の端末で会計済み
				if (!userDAO.checkCart((Integer) session.getAttribute("userId"))) {
					session.setAttribute("errorMessage", "お会計時に不具合が発生しました。");
					response.sendRedirect("cart");
					return;
				}
				ArrayList<Integer> dbCart = cartDAO.selectItemId((Integer) session.getAttribute("userId"));
				ArrayList<Integer> compareCart = new ArrayList<>();
				for (Integer item : dbCart) {
					if (cart.contains(item)) {
						compareCart.add(cart.remove(cart.indexOf(item)));
					}
				}
				if (dbCart.size() == compareCart.size()) {
					Integer accounting = cartDAO.selectAccounting((Integer)session.getAttribute("userId"));
					ArrayList<CouponDTO> coupon = userDAO.selectCoupon((Integer)session.getAttribute("userId"));
					request.setAttribute("accounting", accounting);
					request.setAttribute("coupon", coupon);
					request.getRequestDispatcher("WEB-INF/jsp/confirmDeliveryDate.jsp").forward(request, response);
					
				} else {
					session.setAttribute("errorMessage", "カート内の商品に問題が発生しました。確認の上、再度レンタルの手続きをお願いします");
					response.sendRedirect("cart");
				}
			} catch (SQLException |SQLRuntimeException e) {
				request.getRequestDispatcher("WEB-INF/jsp/systemError.jsp").forward(request, response);
			}
		}
	}

}
