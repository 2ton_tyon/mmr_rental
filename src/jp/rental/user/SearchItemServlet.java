package jp.rental.user;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.rental.DataSourceManager;
import jp.rental.SQLRuntimeException;
import jp.rental.Validate;
import jp.rental.dao.CategoryDAO;
import jp.rental.dao.ItemDAO;
import jp.rental.dto.ItemDTO;

/**
 * Servlet implementation class SearchItemServlet
 */
@WebServlet("/Search")
public class SearchItemServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		int page = 1;
		if(Validate.isStrParseInt(request.getParameter("page"))){
			page = Integer.parseInt(request.getParameter("page"));
		}
		String searchCategory = request.getParameter("category");
		String searchGenre = request.getParameter("genre");
		String searchWord = request.getParameter("search");
		
		try (Connection connection = DataSourceManager.getConnection()) {
			
			if (session.getAttribute("category") == null) {
				CategoryDAO categoryDAO = new CategoryDAO(connection);
				ArrayList<String> categoryList = categoryDAO.selectCategory();
				HashMap<String, ArrayList<String>> list = new HashMap<>();
				for(String category : categoryList) {
					list.put(category, categoryDAO.selectGenreByCategory(category));
				}
				session.setAttribute("category", categoryList);
				session.setAttribute("allGenreList", list);
			}
			
			ItemDAO itemDAO = new ItemDAO(connection);
			ArrayList<ItemDTO> itemList = itemDAO.selectSearchItem(page,searchCategory, searchGenre, searchWord);
			int cntItem = itemDAO.countSearchItem(searchCategory, searchGenre, searchWord);
			request.setAttribute("page", page);
			request.setAttribute("cntItem", cntItem);
			request.setAttribute("itemList", itemList);
			request.setAttribute("category", searchCategory);
			request.setAttribute("genre", searchGenre);
			request.setAttribute("title", searchWord);
			request.getRequestDispatcher("WEB-INF/jsp/searchItem.jsp").forward(request, response);	
		} catch (SQLException |SQLRuntimeException e) {
			request.getRequestDispatcher("WEB-INF/jsp/systemError.jsp").forward(request, response);
		}
		
	}

}
