package jp.rental.user;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.rental.DataSourceManager;
import jp.rental.JavaMail;
import jp.rental.SQLRuntimeException;
import jp.rental.Validate;
import jp.rental.dao.CategoryDAO;
import jp.rental.dao.UserDAO;

/**
 * Servlet implementation class SendMailServlet
 */
@WebServlet("/sendMail")
public class SendMailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("userId") != null) {
			//　ログインしているユーザは再登録できない
			response.sendRedirect("top");
			return;
		}
		try (Connection connection = DataSourceManager.getConnection()) {		
			if (session.getAttribute("category") == null) {
				CategoryDAO categoryDAO = new CategoryDAO(connection);
				ArrayList<String> categoryList = categoryDAO.selectCategory();
				HashMap<String, ArrayList<String>> list = new HashMap<>();
				for(String category : categoryList) {
					list.put(category, categoryDAO.selectGenreByCategory(category));
				}
				session.setAttribute("category", categoryList);
				session.setAttribute("allGenreList", list);
			}
		} catch (SQLException e) {
			// DBとの接続に失敗
			request.getRequestDispatcher("WEB-INF/jsp/systemError.jsp").forward(request, response);
			return;
		}
		request.getRequestDispatcher("WEB-INF/jsp/reRegist.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("userId") != null) {
			//　ログインしているユーザは再登録できない
			response.sendRedirect("top");
			return;
		}
		String mail = request.getParameter("mail");
		
		if (Validate.isEmpty(mail)) {
			request.setAttribute("errorMessage", "入力されたメールアドレスは登録されておりません");
			request.getRequestDispatcher("WEB-INF/jsp/reRegist.jsp").forward(request, response);
			return;
		}
		try (Connection connection = DataSourceManager.getConnection()) {
			UserDAO userDAO = new UserDAO(connection);
			if (userDAO.selectByMailAddress(mail) != null) {
				Integer userId = userDAO.selectByMailAddress(mail);
				StringBuffer content = new StringBuffer();
				content.append("http://192.168.2.11:8080/MMR_RENTAL/reregistPassword?code=");
				content.append(userId);
				content.append("\n");
				content.append("上記のURLからアクセスしてパスワードを再設定してください。");
				JavaMail mailSend = new JavaMail();
			    mailSend.send("MMR ONLINE パスワード再登録の案内", content.toString(), mail);
			    request.setAttribute("message", "入力された宛先にメールを送信しました。確認してください。");
			    userDAO.deletePassword(userId);
			    request.getRequestDispatcher("WEB-INF/jsp/reRegist.jsp").forward(request, response);
			} else {
				request.setAttribute("errorMessage", "入力されたメールアドレスは登録されておりません");
				request.getRequestDispatcher("WEB-INF/jsp/reRegist.jsp").forward(request, response);
			}
		} catch (SQLException | SQLRuntimeException e) {
			request.getRequestDispatcher("WEB-INF/jsp/systemError.jsp").forward(request, response);
		}
		
	}

}
