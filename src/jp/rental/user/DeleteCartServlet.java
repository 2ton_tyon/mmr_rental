package jp.rental.user;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.rental.Cast;
import jp.rental.DataSourceManager;
import jp.rental.SQLRuntimeException;
import jp.rental.Validate;
import jp.rental.dao.CartDAO;

/**
 * Servlet implementation class DeleteCartServlet
 */
@WebServlet("/delete")
public class DeleteCartServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("cart");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		String itemId = request.getParameter("itemId");
		if (session.getAttribute("userId") == null) {
			// ログインしていないユーザ
			ArrayList<Integer> cartList = Cast.cast(session, "cart");
			if (cartList.contains(Integer.parseInt(itemId))) {
				cartList.remove(cartList.indexOf(Integer.parseInt(itemId)));
				if (cartList.size() == 0) {
					session.removeAttribute("cart");
				} else {
					session.setAttribute("cart", cartList);
				}
			}
			
		} else {
			// ログインしているユーザ
			Integer userId = (Integer) session.getAttribute("userId");
			try (Connection connection = DataSourceManager.getConnection()) {
				CartDAO cartDAO = new CartDAO(connection);
				if (!Validate.isStrParseInt(itemId)) {
					request.getRequestDispatcher("WEB-INF/jsp/systemError.jsp").forward(request, response);
					return;
				}
				cartDAO.deleteItem(Integer.parseInt(itemId), userId);
				ArrayList<Integer> cartList = cartDAO.selectItemId(userId);
				if (cartList.size() == 0) {
					session.setAttribute("cart", cartList);
				}
			} catch (SQLException | SQLRuntimeException e) {
				request.getRequestDispatcher("WEB-INF/jsp/systemError.jsp").forward(request, response);
				return;
			}
		}
		response.sendRedirect("cart");
	}

}
