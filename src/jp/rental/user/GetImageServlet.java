package jp.rental.user;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.SQLException;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.rental.DataSourceManager;
import jp.rental.SQLRuntimeException;
import jp.rental.dao.ItemDAO;

/**
 * Servlet implementation class GetImageServlet
 */
@WebServlet("/getImage")
public class GetImageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
      

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 画像のIDを取得する
		String id = request.getParameter("id");
		
		try (Connection connection = DataSourceManager.getConnection()) {
			// 取得したIDに紐づく画像データをDBから取得する
			ItemDAO dao = new ItemDAO(connection);
			BufferedImage img = dao.selectImageById(Integer.parseInt(id));
			// もし画像がなかったらノーイメージ画像を表示する
			if (img == null) {
				String realPath = this.getServletContext().getRealPath("/img/NoImage.png");
				img = ImageIO.read(new File(realPath));
			}
			// 画像をクライアントに返却する
			response.setContentType("image/jpeg");
			OutputStream os = response.getOutputStream();
			ImageIO.write(img, "jpg", os);
			os.flush();
		} catch (SQLException | SQLRuntimeException e) {
			request.getRequestDispatcher("WEB-INF/jsp/systemError.jsp").forward(request, response);
		}
	}

}
