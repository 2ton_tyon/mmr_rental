package jp.rental.user;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.rental.DataSourceManager;
import jp.rental.SQLRuntimeException;
import jp.rental.dao.UserDAO;
import jp.rental.dto.UserDTO;

/**
 * Servlet implementation class MyPageServlet
 */
@WebServlet("/user_page")
public class MyPageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
				
		if (session.getAttribute("userId") == null) {
			//　ログインしていないユーザ
			response.sendRedirect("top");
			return;
		}
		
		Integer userId = (Integer) session.getAttribute("userId");
		
		try (Connection connection = DataSourceManager.getConnection()) {
			UserDAO userDAO = new UserDAO(connection);
			UserDTO userDTO = userDAO.selectByAllInfo(userId);
			request.setAttribute("user", userDTO);
			request.getRequestDispatcher("WEB-INF/jsp/mypage.jsp").forward(request, response);
		} catch (SQLException | SQLRuntimeException e) {
			request.getRequestDispatcher("WEB-INF/jsp/systemError.jsp").forward(request, response);
		}
	}

}
