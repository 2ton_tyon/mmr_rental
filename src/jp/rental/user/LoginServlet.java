package jp.rental.user;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.rental.Cast;
import jp.rental.DataSourceManager;
import jp.rental.SQLRuntimeException;
import jp.rental.Validate;
import jp.rental.dao.CartDAO;
import jp.rental.dao.CategoryDAO;
import jp.rental.dao.UserDAO;
import jp.rental.dto.UserDTO;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		try (Connection connection = DataSourceManager.getConnection()) {		
			if (session.getAttribute("category") == null) {
				CategoryDAO categoryDAO = new CategoryDAO(connection);
				ArrayList<String> categoryList = categoryDAO.selectCategory();
				HashMap<String, ArrayList<String>> list = new HashMap<>();
				for(String category : categoryList) {
					list.put(category, categoryDAO.selectGenreByCategory(category));
				}
				session.setAttribute("category", categoryList);
				session.setAttribute("allGenreList", list);
			}
		} catch (SQLException |SQLRuntimeException e) {
			// DBとの接続に失敗
			request.getRequestDispatcher("WEB-INF/jsp/systemError.jsp").forward(request, response);
			return;
		}
		
		if (session.getAttribute("userId") == null) {
			request.getRequestDispatcher("WEB-INF/jsp/login.jsp").forward(request, response);
		} else {
			response.sendRedirect("top");
		}
	}

	/**
	 * @return 
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session= request.getSession();
		String userId = request.getParameter("userId");
		String password =request.getParameter("password");
		if (Validate.isEmpty(userId) && Validate.isEmpty(password)) {
			//login失敗
			request.setAttribute("errorMessage","メールアドレス、パスワードは入力必須項目です");
			request.getRequestDispatcher("WEB-INF/jsp/login.jsp").forward(request, response);
			return;
		}
		
		try (Connection connection = DataSourceManager.getConnection()) {
			UserDAO userDAO = new UserDAO(connection);
			UserDTO userDTO = userDAO.selectUser(userId, password);
			if (userDTO.getUserId() != null) {
				//login成功
				session.setAttribute("userId", userDTO.getUserId());
				
				if (session.getAttribute("cart") != null) {
					// 未ログイン中にカートに追加した商品をユーザのカートテーブルに追加する
					ArrayList<Integer> cart = Cast.cast(session, "cart");
					for (Integer itemId : cart) {
						CartDAO cartDAO = new CartDAO(connection);
						cartDAO.insertItem(itemId, userDTO.getUserId());
					}
				}
				// ログインしたユーザが現在カート内に商品を入れているか確認する
				if (session.getAttribute("cart") == null && userDAO.checkCart(userId)) {
					session.setAttribute("cart", true);
				}
				response.sendRedirect("top");
				
			} else {
				//login失敗
				request.setAttribute("errorMessage","メールアドレス、またはパスワードが間違っています。");
				request.getRequestDispatcher("WEB-INF/jsp/login.jsp").forward(request, response);
			}
		} catch (SQLException | SQLRuntimeException e) {
			// DBとの接続に失敗
			request.getRequestDispatcher("WEB-INF/jsp/systemError.jsp").forward(request, response);
		}
		
	}

}
