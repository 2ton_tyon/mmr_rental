package jp.rental.user;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.rental.Cast;
import jp.rental.DataSourceManager;
import jp.rental.SQLRuntimeException;
import jp.rental.dao.CartDAO;
import jp.rental.dao.CategoryDAO;
import jp.rental.dao.ItemDAO;
import jp.rental.dto.ItemDTO;

/**
 * Servlet implementation class CartViewServlet
 */
@WebServlet("/cart")
public class CartViewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		
		try (Connection connection = DataSourceManager.getConnection()) {
			
			if (session.getAttribute("category") == null) {
				CategoryDAO categoryDAO = new CategoryDAO(connection);
				ArrayList<String> categoryList = categoryDAO.selectCategory();
				HashMap<String, ArrayList<String>> list = new HashMap<>();
				for(String category : categoryList) {
					list.put(category, categoryDAO.selectGenreByCategory(category));
				}
				session.setAttribute("category", categoryList);
				session.setAttribute("allGenreList", list);
			}
			
			if (session.getAttribute("userId") == null) {
				// ログインしていないユーザはセッションからカート情報の取得
				if(session.getAttribute("cart")!= null) {
					ArrayList<Integer> cartList = Cast.cast(session, "cart");
					if (cartList.size() > 0) {
						ItemDAO itemDAO = new ItemDAO(connection);
						ArrayList<ItemDTO> itemList = itemDAO.selectCartList(cartList);
						request.setAttribute("itemList", itemList);
					}
				}
			} else {
				// ログインしているユーザに関してはDBからカート内の情報を取得する
				Integer userId = (Integer) session.getAttribute("userId");
				CartDAO cartDAO = new CartDAO(connection);
				ArrayList<Integer> cartList = cartDAO.selectItemId(userId);
				if (cartList.size() > 0) {
					ItemDAO itemDAO = new ItemDAO(connection);
					ArrayList<ItemDTO> itemList = itemDAO.selectCartList(cartList);
					request.setAttribute("itemList", itemList);
				}
			}
		} catch (SQLException | SQLRuntimeException e) {
			request.getRequestDispatcher("WEB-INF/jsp/systemError.jsp").forward(request, response);
		}
		request.getRequestDispatcher("WEB-INF/jsp/checkCart.jsp").forward(request, response);
			
	}

}
