package jp.rental;

import java.sql.Date;

public class Validate {
	
	/** 
	 * 入力した文字列が空文字またはNullか確認する
	 * @param str
	 * @return 
	 */
	public static boolean isEmpty(String str) {
		if (str == null || "".equals(str)) {
			return true;
		}
		return false;
	}
	
	
	/**
	 * 入力した文字列配列のうち、1つでもNullまたは空文字があったらTrue
	 * @param str
	 * @return
	 */
	public static boolean isEmptyList(String[] str) {
		for (String s : str) {
			if (s == null || "".equals(s)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 入力した文字列が数値に変換可能か確認する
	 * @param str
	 * @return
	 */
	public static boolean isStrParseInt(String str) {
		if (isEmpty(str)) {
			return false;
		}
		try {
			Integer.parseInt(str);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
	
	/**
	 * 入力した文字列配列のうちすべての文字列が数値に変換可能か確認
	 * 1つでも変換に失敗するとFalse、すべて成功するとtrue
	 * @param str
	 * @return
	 */
	public static boolean isStrParseIntList(String[] str) {
		if (str == null) {
			return false;
		}
		for (int i = 0; i < str.length; i++){
			if (isEmpty(str[i])) {
				try {
					Integer.parseInt(str[i]);
				} catch (NumberFormatException e) {
					return false;
				}
			}
		}
		return true;
	}
	
	/**
	 * 入力した文字列が日付型に変換可能か確認する
	 * @param str
	 * @return
	 */
	public static boolean isStrParseDate(String str) {
		if (isEmpty(str)) {
			return false;
		}
		try {
			Date.valueOf(str);
		} catch (IllegalArgumentException e) {
			return false;
		}
		return true;
	}
	
	/**
	 * 入力した文字列とパターンがマッチするかを確認する
	 * @param str
	 * @param pattern マッチングする文字列パターン
	 * @return
	 */
	public static boolean isRgularExpression(String str, String pattern) {
		if (isEmpty(str)) {
			return false;
		}
		
		if (str.matches(pattern)) {
			return true;
		}
		return false;
	}
}