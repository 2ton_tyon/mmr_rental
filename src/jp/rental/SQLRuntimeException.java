package jp.rental;

import java.io.IOException;
import java.sql.SQLException;

public class SQLRuntimeException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	/*
	 * SQLの実行時例外を返す自作のクラス
	 */
	public SQLRuntimeException(SQLException e) {
		super(e);
	}
	
	public SQLRuntimeException(IOException e) {
		super(e);
	}
}
