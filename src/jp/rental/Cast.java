package jp.rental;

import java.util.ArrayList;

import javax.servlet.http.HttpSession;

public class Cast {
	
	@SuppressWarnings("unchecked")
	public static ArrayList<Integer> cast (HttpSession session, String str) {
		return (ArrayList<Integer>) session.getAttribute(str);
	}
}
