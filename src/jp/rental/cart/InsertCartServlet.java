package jp.rental.cart;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.rental.Cast;
import jp.rental.DataSourceManager;
import jp.rental.SQLRuntimeException;
import jp.rental.Validate;
import jp.rental.dao.CartDAO;
import jp.rental.dao.ItemDAO;

/**
 * Servlet implementation class InsertCartServlet
 */
@WebServlet("/add")
public class InsertCartServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("top");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		String query = request.getQueryString();
		
		String itemId = request.getParameter("id");
		Integer userId = (Integer) session.getAttribute("userId");
		if (!Validate.isStrParseInt(itemId)) {
			// 不正なItemIDが入力されたとき
			request.getRequestDispatcher("WEB-INF/jsp/systemError.jsp").forward(request, response);
			return;
		}
		
		// 入力された商品IDが存在するか確認する
		try (Connection connection = DataSourceManager.getConnection()) {
			ItemDAO itemDAO = new ItemDAO(connection);
			if (!itemDAO.isItem(Integer.parseInt(itemId))) {
				// 存在しないItemIDが入力された
				request.getRequestDispatcher("WEB-INF/jsp/systemError.jsp").forward(request, response);
				return;
			}
			
			if (userId == null) {
				//　ログインしていないユーザ
				ArrayList<Integer> cartList = new ArrayList<>();
				if(session.getAttribute("cart") == null) {
					cartList.add(Integer.parseInt(itemId));
				} else {
					cartList = Cast.cast(session, "cart");
					cartList.add(Integer.parseInt(itemId));
				}
				session.setAttribute("cart", cartList);
			} else {
				CartDAO cartDAO = new CartDAO(connection);
				cartDAO.insertItem(Integer.parseInt(itemId), userId);
				session.setAttribute("cart", true);
			}
			
		} catch (SQLException | SQLRuntimeException e) {
			request.getRequestDispatcher("WEB-INF/jsp/systemError.jsp").forward(request, response);
			return;
		}
		
		try {
			StringBuffer sb = new StringBuffer();
			sb.append(new URI(request.getHeader("referer")).getPath());
			sb.append("?");
			sb.append(query);
			response.sendRedirect(sb.toString());
		} catch (URISyntaxException e) {
			request.getRequestDispatcher("WEB-INF/jsp/systemError.jsp").forward(request, response);
		}
		
	}

}
