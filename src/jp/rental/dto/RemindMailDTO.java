package jp.rental.dto;

public class RemindMailDTO {
	
	private String mailAddress;
	public String getMailAddress() {
		return mailAddress;
	}
	public void setMailAddress(String mailAddress) {
		this.mailAddress = mailAddress;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getRemindTime() {
		return remindTime;
	}
	public void setRemindTime(String remindTime) {
		this.remindTime = remindTime;
	}
	public String getPassCode() {
		return passCode;
	}
	public void setPassCode(String passCode) {
		this.passCode = passCode;
	}
	public String getCommonKey() {
		return commonKey;
	}
	public void setCommonKey(String commonKey) {
		this.commonKey = commonKey;
	}
	public String getUsedFlg() {
		return usedFlg;
	}
	public void setUsedFlg(String usedFlg) {
		this.usedFlg = usedFlg;
	}
	private String code;
	private String remindTime;
	private String passCode;
	private String commonKey;
	private String usedFlg;

}
