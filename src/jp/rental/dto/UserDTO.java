package jp.rental.dto;

import java.sql.Date;
import java.util.ArrayList;

public class UserDTO {
	
	private Integer userId;
	private String userName;
	private String password;
	private String mailAddress;
	private String postalCode;
	private String address1;
	private String address2;
	private String tel;
	private Date birthdayDate;
	private String cardNumber;
	private String cardExpirationDate;
	private String cardSecurityCode;
	private String inviteCodeFriend;
	private String inviteCode;
	private int deleteFlag;
	private ArrayList<CouponDTO> coupons;
	
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getMailAddress() {
		return mailAddress;
	}
	public void setMailAddress(String mailAddress) {
		this.mailAddress = mailAddress;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public Date getBirthdayDate() {
		return birthdayDate;
	}
	public void setBirthdayDate(Date birthdayDate) {
		this.birthdayDate = birthdayDate;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getCardExpirationDate() {
		return cardExpirationDate;
	}
	public void setCardExpirationDate(String cardExpirationDate) {
		this.cardExpirationDate = cardExpirationDate;
	}
	public String getCardSecurityCode() {
		return cardSecurityCode;
	}
	public void setCardSecurityCode(String cardSecurityCode) {
		this.cardSecurityCode = cardSecurityCode;
	}
	public String getInviteCodeFriend() {
		return inviteCodeFriend;
	}
	public void setInviteCodeFriend(String inviteCodeFriend) {
		this.inviteCodeFriend = inviteCodeFriend;
	}
	public String getInviteCode() {
		return inviteCode;
	}
	public void setInviteCode(String inviteCode) {
		this.inviteCode = inviteCode;
	}
	public int getDeleteFlag() {
		return deleteFlag;
	}
	public void setDeleteFlag(int deleteFlag) {
		this.deleteFlag = deleteFlag;
	}
	
	public ArrayList<CouponDTO> getCoupons() {
		return coupons;
	}
	public void setCoupons(ArrayList<CouponDTO> coupons) {
		this.coupons = coupons;
	}
	
}