package jp.rental.dto;

import java.sql.Date;

public class RentalDTO {
	
	private Integer userId;
	private Integer useCouponId;
	private Date deliveryDate;
	private Integer deliveryTimeId;
	
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getUseCouponId() {
		return useCouponId;
	}
	public void setUseCouponId(Integer useCouponId) {
		this.useCouponId = useCouponId;
	}
	public Date getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public Integer getDeliveryTimeId() {
		return deliveryTimeId;
	}
	public void setDeliveryTimeId(Integer deliveryTimeId) {
		this.deliveryTimeId = deliveryTimeId;
	}

}
