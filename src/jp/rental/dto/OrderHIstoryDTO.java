package jp.rental.dto;

import java.sql.Date;
import java.util.ArrayList;

public class OrderHIstoryDTO {
	private Integer rental_number;
	private Date orderDate;
	private Date returnDate;
	private Integer itemSum;
	private Integer priceSum;
	// お会計金額
	private int fee;
	private String status;
	private ArrayList<ItemDTO> item;
	public Integer getRental_number() {
		return rental_number;
	}
	public void setRental_number(Integer rental_number) {
		this.rental_number = rental_number;
	}
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	public Date getReturnDate() {
		return returnDate;
	}
	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}
	public Integer getItemSum() {
		return itemSum;
	}
	public void setItemSum(Integer itemSum) {
		this.itemSum = itemSum;
	}
	public Integer getPriceSum() {
		return priceSum;
	}
	public void setPriceSum(Integer priceSum) {
		this.priceSum = priceSum;
	}
	public int getFee() {
		return fee;
	}
	public void setFee(int fee) {
		this.fee = fee;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public ArrayList<ItemDTO> getItem() {
		return item;
	}
	public void setItem(ArrayList<ItemDTO> item) {
		this.item = item;
	}
	
	
	
}
	