package jp.rental.dto;

import java.util.ArrayList;

public class CartDTO {
	
	private Integer cartNumber;
	private ArrayList<Integer> cart;
	private String userName;
	
	public Integer getCartNumber() {
		return cartNumber;
	}
	public void setCartNumber(Integer cartNumber) {
		this.cartNumber = cartNumber;
	}
	public ArrayList<Integer> getCart() {
		return cart;
	}
	public void setCart(ArrayList<Integer> cart) {
		this.cart = cart;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
}
